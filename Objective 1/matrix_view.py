"""
matrix_view
=================

Tools to show an array or array-like object in a new window, or inspect a
column or row.

Depends on:
    - tkinter (should be available on a standard python distribution) 
    - pandas (passed object gets converted into a pandas.DataFrame)
    - numpy
    - scipy.sparse

Methods:
    show(array_like_object): displays matix in separate window

    inspect_[col|row|vect](): displays a column/row/vector without null entries
                or NaN entries, optionally concatenated to a labels table for
                metadata (e.g. name, unit, etc.)

Unit tests:
    Unit tests are defined for inspect_[col|row|vect]. Please run the following
    command after any modification to make sure that this modification does not
    break an existing functionality:
       python -m unittest -v matrix_view.py Tests.test_matrix_view

Various keyboard shortcuts are available - see matrix_view.show
Recommend abbreviation: mv

Author: KST konstantin.stadler@ntnu.no
Author: GMB guillaume.majeau-bettez@ntnu.no

"""

# imports

import sys
import pandas as pd
import numpy as np
import scipy.sparse as sp


if sys.version_info.major < 3: 
    import Tkinter as tk 
else:
    import tkinter as tk 

class _SaveJump():
    """ Structure to save jump position in the text field """
    row = 0
    col = 0
    def set_col(c):
        _SaveJump.col = c
    def set_row(r):
        _SaveJump.row = r

class _MatrixViewer(tk.Frame):
    """ GUI to visualize the matrix """
  
    def __init__(self, txt_data, master = None):
        
        self.master = master
        self.txt_data = txt_data
        tk.Frame.__init__(self,master)

        self.screenwidth = master.winfo_screenwidth()
        self.screenheight = master.winfo_screenheight()

        self.initUI()

    def initUI(self):
        """ Set up the matrix view """

        self.master.title("Matrix Viewer")

        #quitButton = tk.Button(self, text="Quit", command=self.master.destroy)

        self.master.bind('<Escape>', lambda e: self.master.destroy())
        self.master.bind('<Return>', lambda e: self.master.destroy())
        self.master.bind('x', lambda e: self.master.destroy())

        xscrollbar = tk.Scrollbar(self, orient=tk.HORIZONTAL)
        yscrollbar = tk.Scrollbar(self)

        tt_data = tk.Text(self, wrap = tk.NONE,
                width=self.screenwidth, height=self.screenheight,
                xscrollcommand=xscrollbar.set,
                yscrollcommand=yscrollbar.set)

        tt_data.insert(tk.INSERT, self.txt_data)

        xscrollbar.config(command=tt_data.xview)
        yscrollbar.config(command=tt_data.yview)

        tt_data['state'] = tk.DISABLED

        tt_data.grid(row = 0, column = 0, sticky = 'nesw')   
        yscrollbar.grid(row=0,column=1, sticky='ns')
        xscrollbar.grid(row=2,column=0, sticky='we')

        #quitButton.grid(row = 3, column = 0)
        
        self.master.bind('j', lambda e: tt_data.yview_scroll(20, 'units'))
        self.master.bind('k', lambda e: tt_data.yview_scroll(-20, 'units'))
        self.master.bind('l', lambda e: tt_data.xview_scroll(20, 'units'))
        self.master.bind('h', lambda e: tt_data.xview_scroll(-20, 'units'))
        
        self.master.bind('d', lambda e: (tt_data.xview_scroll(20, 'units'), tt_data.yview_scroll(20, 'units')))
        self.master.bind('D', lambda e: (tt_data.xview_scroll(-20, 'units'), tt_data.yview_scroll(-20, 'units')))
        self.master.bind('<space>', lambda e: (tt_data.xview_moveto(0), tt_data.yview_moveto(0)))
        
        self.master.bind('c', lambda e: (_SaveJump.set_col(tt_data.yview()), tt_data.yview_moveto(0)))
        self.master.bind('r', lambda e: (_SaveJump.set_row(tt_data.xview()), tt_data.xview_moveto(0)))
        self.master.bind('C', lambda e: tt_data.yview_moveto(_SaveJump.col[0]))
        self.master.bind('R', lambda e: tt_data.xview_moveto(_SaveJump.row[0]))

        self.master.bind('<Down>', lambda e: tt_data.yview_scroll(20, 'units'))
        self.master.bind('<Up>', lambda e: tt_data.yview_scroll(-20, 'units'))
        self.master.bind('<Right>', lambda e: tt_data.xview_scroll(20, 'units'))
        self.master.bind('<Left>', lambda e: tt_data.xview_scroll(-20, 'units'))

        self.columnconfigure(0,weight = 1)
        self.columnconfigure(1,weight = 0)
        self.rowconfigure(0,weight = 1)
        self.rowconfigure(1,weight = 0)
        #self.rowconfigure(2,weight = 0)
        self.pack()

def show(data, max=False):
    """ Shows a array or array like object in a new window

    Array like objects are for example 
    numpy 2d array, lists, sets, DataFrames,... 
    (everything which can be converted into a pandas.DataFrame)

    if max=True : full screen
    
    Keyboard shortcuts:
    
    Return      Exit
    Escape      Exit
    Arrow Keys  Move
    hjkl        Move (as in Vim)
    d,D         Move up/down diagonally
    c,r         Jump to the first row/column (show the header)
    C,R         Jump back to the data view before c or r
    """
    dd = pd.DataFrame(data)
    root = tk.Tk()
    x=root.winfo_screenwidth()
    y=root.winfo_screenheight()
    root.geometry('{}x{}+10+10'.format(str(int(x/2)), str(int(y/2))))
    app = _MatrixViewer(txt_data = dd.to_string(), master = root)
    if max: root.wm_state('zoomed')
    root.focus_force()
    root.mainloop()  


def inspect_col(a, col,
                labels=None, keep_index=True, verbose=True, doshow=False):
    """ Displays a column of an array, sans null or NaN entries, with labels.

    Function tested for following array formats:
        * Numpy array
        * scipy sparse matrix
        * Pandas Dataframe
        * Pandas SparseDataframe

    Args:
    -----
        a: matrix of interest

        column: index of column of interest

        labels: [numpy array] labels of rows, to be concatenated with the
            cleaned up column
                + Function should automatically detect if \labels needs to be
                transposed, except if \labels is a square array, in which case
                one must make sure that each entry is described in a new *row*

        keep_index: [boolean] with pandas dataframe, print internal indexes
            (either with labels or instead of labels), or else get rid of them

        verbose: [boolean] print cleaned up column

        doshow: [boolean] show cleaned up column in separate window

    Returns:
    --------
        aclean : numpy array, sans null or NaN values and possibly with labels.

    """

    # GET COLUMN
    try:
        # If sparse matrix or numpy array
        acol = a[:, col]
    except:
        try:
            # If pandas dataframe
            acol = a.ix[:, col]
        except:
            # pandas dataframe without .ix support? old version?
            acol = a[col]

    return inspect_vect(acol, labels, keep_index, verbose, doshow)


def inspect_row(a, row,
                labels=None, keep_index=True, verbose=True, doshow=False):
    """ Displays a row of a matrix, without null or NaN entries, with labels.

    Matrix can be in any of the following formats:
        * Numpy array
        * scipy sparse matrix
        * Pandas Dataframe
        * Pandas SparseDataframe

    Args:
    -----
        a: matrix of interest

        row: index of row of interest

        labels: [numpy array] labels of columns, to be concatenated with the
            cleaned up row.
                + Function should automatically detect if \labels needs to be
                transposed, except if \labels is a square array, in which case
                one must make sure that each entry is described in a new *row*

        keep_index: [boolean] with pandas dataframe, print internal indexes
            (either with labels or instead of labels), or else get rid of them

        verbose: [boolean] print cleaned up row

        doshow: [boolean] show cleaned up row in separate window

    Returns:
    --------
        aclean : numpy array, sans null or NaN values and possibly with labels.

    """

    # GET ROW
    try:
        # If sparse matrix or numpy array
        arow = a[row, :]
    except:
        try:
            # If pandas dataframe
            arow = a.ix[row, :]
        except:
            # Selecting rows in SparseDataFrames seems not fully implemented,
            # so we try two workarounds with transpose
            try:
                arow = a.T.ix[:, row]
            except:
                arow = a.T[row]

    return inspect_vect(arow, labels, keep_index, verbose, doshow)


def inspect_vect(a, labels=None, keep_index=True, verbose=True, doshow=False):
    """ Displays a vector, without null or NaN entries, with labels.

    Matrix can be in any of the following formats:
        * Numpy array
        * scipy sparse matrix
        * Pandas Dataframe
        * Pandas SparseDataframe
        * Pandas Series

    Can be used directly, but also serves as backend for inspect_col() and
    inspect_row()

    Args:
    -----
        a: vector of interest

        labels: [numpy array] labels, to be concatenated with the cleaned up
            vector
                + Function should automatically detect if \labels needs to be
                transposed, except if \labels is a square array, in which case
                one must make sure that each entry is described in a new *row*

        keep_index: [boolean] with pandas dataframe, print internal indexes
            (either with labels or instead of labels), or else get rid of them

        verbose: [boolean] print cleaned up vector

        doshow: [boolean] show cleaned up vector in separate window

    Returns:
    --------
        aclean : numpy array, sans null or NaN values and possibly with labels.

    """

    # MAKE VECTOR DENSE, if not already
    try:
        a = np.array(a.todense())
    except:
        try:
            a = a.to_dense()
        except:
            pass  # not sparse

    # DEAL WITH "FALSE" VECTORS
    if (a.ndim == 2) and (min(a.shape) == 1):
        avect = a.squeeze()
    else:
        avect = a

    # PREPARE BOOLEAN (numpy) VECTOR TO SELECT NON-NULL NON-NAN ENTRIES
    bo = np.array((avect != 0) & (~np.isnan(avect))).squeeze()

    # REMOVE NULL/NAN ENTRIES, MAKE NUMPY ARRAY
    if keep_index:   # Try to obtain Pandas index values...
        try:
            aclean = avect[bo].reset_index().values
        except:
            aclean = np.array(avect[bo])
    else:  # ... or not.
            aclean = np.array(avect[bo])

    # IF LABELS,  CONCATENATE
    if labels is not None:
        labels = labels.astype(object)
        try:
            # 2D labels
            aclean = np.c_[labels[bo, :], aclean]
        except:
            try:
                # Maybe labels need to be transposed
                aclean = np.c_[labels[:, bo].T, aclean]
            except:
                # 1D labels
                aclean = np.c_[labels[bo], aclean]

    # PRINT AND RETURN
    if verbose:
        print(aclean)

    if doshow:
        show(aclean)

    return aclean
