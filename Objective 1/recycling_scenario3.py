#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import gzip
import pickle
from pathlib import Path
import matrix_view
import core
from scipy import sparse
# Linear algebra solver for _dense_ datasets (numpy array, etc.)
from scipy.linalg import solve
# scipy sparse matrix formats and solvers
from scipy.sparse import csc_matrix, csr_matrix
from scipy.sparse.linalg import spsolve
import jenkspy
import matplotlib
import matplotlib.pyplot as plt
import json
import plotly.express as px
import scipy.cluster.vq
import math
import matplotlib.ticker as mtick


# # Scenario results class

# In[2]:


class scenario_results:
# Generate LCA results for one specified scenario.
#___________________________________________________________________________
# USEFUL METHODS
#___________________________________________________________________________ 
    def _sp_loc(self, df, index, columns, val):
        """Modifies value in Sparse DataFrame.
        
        Parameters:
        df: DataFrame to modify
        index: Name of the index containing the value to modify
        columns: Name of the column containing the value to modify
        val: New value
        
        Returns:
        df: Modified DataFrame
        
        """

        # Save the original sparse format for reuse later
        spdtypes = df.dtypes[columns]
        # Convert concerned Series to dense format
        df[columns] = df[columns].sparse.to_dense()
        # Do a normal insertion with .loc[]
        df.loc[index, columns] = val
        # Back to the original sparse format
        df[columns] = df[columns].astype(spdtypes)

        return df
    
#___________________________________________________________________________
    def _trace_graph_comp (self, d_tot):
        """Traces normalized graph to compare d vectors from different scenarios. 
        
        Parameters:
        d_tot: Dataframe containing all d vectors to represent in graph as columns
        
        Returns:
        graph_comp: Normalized graph
        
        """
        # Normalize results
        d_norm = pd.DataFrame(index=d_tot.index, columns = d_tot.columns)
        max_column = (abs(d_tot)).max(axis=1)
        for c in d_tot.columns:
            d_norm.loc[:, c] = d_tot.loc[:,c]/max_column

        # Trace graph
        graph_comp= d_norm.plot(kind='barh',figsize=(10,12), width=0.5, stacked=False, color = ['firebrick','deepskyblue','seagreen','gold'] )
        graph_comp.set_xlim(-1.0,1)
        graph_comp.legend(loc = 'best', bbox_to_anchor = (1.0,0.5))
        plt.gca().xaxis.set_major_formatter(mtick.PercentFormatter(xmax=1.0))

        return graph_comp
    
#___________________________________________________________________________
# INITIALIZATION METHOD
#___________________________________________________________________________       
    def __init__(self):
        """Imports ecoinvent database and world map; initialize LCA matrix; import keys, values and tables from template
        - Shows new processes created

        """
        
        self.meta = ['activityName', 'productName', 'geography', 'unitName']
        
        # GDP reference year
        self.year = str(input("Enter the reference year for GDP data (example: 2018):"))
        
        # Excel template importation
        self.template = str(input("Enter the path to the template file:"))
    
        # Import keys and values from template
        with pd.ExcelFile(self.template) as file:
            self.process_keys = (pd.read_excel(file, 'Process keys', index_col=0).squeeze())
            self.default_keys = (pd.read_excel(file, 'Default keys', index_col=0).squeeze())
            self.delete = (pd.read_excel(file, 'Delete keys', index_col=0).squeeze())
            self.chemicals_keys = (pd.read_excel(file, 'Chemicals keys', index_col=0).squeeze())
            self.init_data = (pd.read_excel(file, 'Initial data', index_col=0).squeeze())
            self.waste_key = self.init_data.loc['Waste treatment process key']
            self.supp_impacts = (pd.read_excel(file, 'Supp. impacts', index_col=0).squeeze())
            self.composition = (pd.read_excel(file, 'Composition', index_col=0).squeeze())
            self.destination = (pd.read_excel(file, 'Destination', index_col=0).squeeze())
            self.efficiency_data = (pd.read_excel(file, 'Efficiency data', index_col=0).squeeze())
            print('Efficiency data DataFrame: self.efficiency_data')
            self.electricity_consumption = (pd.read_excel(file, 'Electricity consumption', index_col=0).squeeze())
            print('Electricity consumption data DataFrame: self.electricity_consumption')
            self.news_electricity_consumption = (pd.read_excel(file, 'Newspaper electricity', index_col=0).squeeze())
            print('Electricity consumption newspaper data DataFrame: self.news_electricity_consumption')
            self.graphic_electricity_consumption = (pd.read_excel(file, 'Graphic electricity', index_col=0).squeeze())
            print('Electricity consumption graphic paper data DataFrame: self.graphic_electricity_consumption')
            self.occ_electricity_consumption = (pd.read_excel(file, 'OCC electricity', index_col=0).squeeze())
            print('Electricity consumption OCC data DataFrame: self.occ_electricity_consumption')
            self.energy_consumption = (pd.read_excel(file, 'Energy consumption', index_col=0).squeeze())
            print('Energy consumption data DataFrame: self.energy_consumption')
            self.news_energy_consumption = (pd.read_excel(file, 'Newspaper energy', index_col=0).squeeze())
            print('Energy consumption newspaper data DataFrame: self.news_energy_consumption')
            self.graphic_energy_consumption = (pd.read_excel(file, 'Graphic energy', index_col=0).squeeze())
            print('Energy consumption graphic paper data DataFrame: self.graphic_energy_consumption')
            self.occ_energy_consumption = (pd.read_excel(file, 'OCC energy', index_col=0).squeeze())
            print('Energy consumption OCC data DataFrame: self.occ_energy_consumption')
            self.biom_consumption = (pd.read_excel(file, 'Biomass consumption', index_col=0).squeeze())
            print('Biomass consumption data DataFrame: self.biom_consumption')
            self.chemicals_consumption = (pd.read_excel(file, 'Chemicals consumption', index_col=0).squeeze())
            print('Chemicals consumption data DataFrame: self.chemicals_consumption')
            self.news_chemicals_consumption = (pd.read_excel(file, 'Newspaper chemicals', index_col=0).squeeze())
            print('Chemicals consumption newspaper data DataFrame: self.news_chemicals_consumption')
            self.graphic_chemicals_consumption = (pd.read_excel(file, 'Graphic chemicals', index_col=0).squeeze())
            print('Chemicals consumption graphic paper data DataFrame: self.graphic_chemicals_consumption')
            self.occ_chemicals_consumption = (pd.read_excel(file, 'OCC chemicals', index_col=0).squeeze())
            print('Chemicals consumption OCC data DataFrame: self.occ_chemicals_consumption')
            self.sludge_treatments = (pd.read_excel(file, 'Sludge treatments', index_col=0).squeeze())
            print('Sludge treatments data DataFrame: self.sludge_treatments')
            self.water_consumption = (pd.read_excel(file, 'Water consumption', index_col=0).squeeze())
            print('Water consumption data DataFrame: self.water_consumption')
            self.news_water_consumption = (pd.read_excel(file, 'Newspaper water', index_col=0).squeeze())
            print('Water consumption newspaper data DataFrame: self.news_water_consumption')
            self.graphic_water_consumption = (pd.read_excel(file, 'Graphic water', index_col=0).squeeze())
            print('Water consumption graphic paper data DataFrame: self.graphic_water_consumption')
            self.occ_water_consumption = (pd.read_excel(file, 'OCC water', index_col=0).squeeze())
            print('Water consumption OCC data DataFrame: self.occ_water_consumption')
            self.water_emissions= (pd.read_excel(file, 'Water emissions', index_col=0).squeeze()) 
            self.news_water_emissions = (pd.read_excel(file, 'Newspaper wat emi', index_col=0).squeeze())
            self.graphic_water_emissions = (pd.read_excel(file, 'Graphic wat emi', index_col=0).squeeze())
            self.occ_water_emissions = (pd.read_excel(file, 'OCC wat emi', index_col=0).squeeze())
            self.geographies = (pd.read_excel(file, 'Geographies', index_col=0).squeeze())
            self.sludge_incineration_A = (pd.read_excel(file, 'Sludge incineration A keys', index_col=0).squeeze())
            self.sludge_incineration_F = (pd.read_excel(file, 'Sludge incineration F keys', index_col=0).squeeze())
            self.new_processes = (pd.read_excel(file, 'New processes', index_col=0).squeeze())
            # Technological level indicators
            ## Recycling system availability in different countries
            self.recyc_rate = (pd.read_excel(file, 'Recycling', index_col=0).squeeze())
            ## PIB/capita for different countries
            self.pib = (pd.read_excel(file, 'PIB', index_col=0).squeeze())
            ## Waste treatment
            self.waste_treatment = (pd.read_excel(file, 'Waste treatment', index_col=0).squeeze()).fillna(0)
            self.waste_treatment_sludge = (pd.read_excel(file, 'Waste treatment sludge', index_col=0).squeeze()).fillna(0)
            ## Waste keys
            self.waste_keys = (pd.read_excel(file, 'Waste keys', index_col=0).squeeze())
            ## Open dump keys
            self.open_dump_keys = (pd.read_excel(file, 'Open dump keys', index_col=0).squeeze())
            ## Infiltration
            self.infiltration = (pd.read_excel(file, 'Infiltration', index_col=0).squeeze())
            # Creation of variables for keys of base recycling process
            ## Initial recycling process key
            self.proc_key = self.process_keys.loc[self.init_data.loc['Product'],'Recycling process key']
            ## Recyclable waste input key
            self.rawmat_key = self.process_keys.loc[self.init_data.loc['Product'],'Recyclable waste input key']
            ## Substituted virgin product key
            self.sub_key = self.process_keys.loc[self.init_data.loc['Product'],'Substituted virgin product key']
            
        # Create self.mix_energ with national energy mix data from U.S. Energy Information Administration Database (csv file)
        with open(self.init_data.loc['Energy mix path']) as csvfile:
            mix_read = pd.read_csv(csvfile, 'Energy mix total', delimiter = ',')
            self.mix_energ = pd.DataFrame(data=mix_read)
            self.mix_energ = self.mix_energ[self.mix_energ.Year== 2019] # Most recent year is chosen
            # Delete data from energy sources that aren't used in paper mills
            self.mix_energ = self.mix_energ.drop('Year',axis=1).drop('Solar Consumption - EJ', axis=1).drop('Hydro Consumption - EJ', axis=1).drop('Nuclear Consumption - EJ', axis=1).drop('Wind Consumption - EJ', axis=1).drop('Geo Biomass Other - EJ', axis=1)
            self.mix_energ=self.mix_energ.fillna(0)
            # Create a new columns with total energy consumption
            for p in self.mix_energ.index:
                self.mix_energ.loc[p,'Total - EJ']= self.mix_energ.loc[p, 'Oil']+self.mix_energ.loc[p, 'Gas']+self.mix_energ.loc[p, 'Coal']+self.mix_energ.loc[p,'Biofuel']*0.0036 #(Biofuels are converted to EJ for total. All other energy categories are already in EJ)
            #Transform data in fraction of total energy used instead of EJ
            for p in self.mix_energ.index:
                if self.mix_energ.loc[p,'Total - EJ']!=0:
                    self.mix_energ.loc[p, 'Oil']=self.mix_energ.loc[p, 'Oil']/self.mix_energ.loc[p, 'Total - EJ']
                    self.mix_energ.loc[p, 'Gas']=self.mix_energ.loc[p, 'Gas']/self.mix_energ.loc[p, 'Total - EJ']
                    self.mix_energ.loc[p, 'Coal']=self.mix_energ.loc[p, 'Coal']/self.mix_energ.loc[p, 'Total - EJ']
                    self.mix_energ.loc[p, 'Biofuel']=self.mix_energ.loc[p, 'Biofuel']/self.mix_energ.loc[p, 'Total - EJ']
            
        # Add data from IEA to self.mix_energ
        if self.init_data.loc['IEA data path']!='no':
            self.no_iea_data='no'
            with pd.ExcelFile(self.init_data.loc['IEA data path']) as file:
                self.iea_results = (pd.read_excel(file, 'iea results', index_col=0).squeeze())
        else:
            self.no_iea_data='yes'
        
        self.geographies['name']=self.geographies.index
        self.geographies2 = self.geographies.set_index('shortname')
        
        if self.no_iea_data=='no':
            # Associate 2 letter country codes from IEA results to 3 letter country codes
            for i in self.iea_results.index:
                if i in self.geographies2.index:
                    country = self.geographies2.loc[i,'name']
                    if country in self.pib.index:
                        country_code = self.pib.loc[country, 'Country code']
                        self.iea_results.loc[i, 'Country code']=country_code
                    else:
                        self.iea_results=self.iea_results.drop(i,axis=0)
                else: 
                    self.iea_results = self.iea_results.drop(i,axis=0)
            self.iea_results.set_index('Country code',inplace=True)

            self.mix_energ=self.mix_energ.set_index('Country code')
            
            # Replace national energy mix data from U.S. Energy Information Administration Database with more precise data from IEA when possible in self.mix_energ
            for i in self.iea_results.index:
                if i in self.mix_energ.index:
                    self.mix_energ.loc[i, 'Oil']=self.iea_results.loc[i, 'Oil']
                    self.mix_energ.loc[i, 'Gas']=self.iea_results.loc[i, 'Gas']
                    self.mix_energ.loc[i, 'Coal']=self.iea_results.loc[i, 'Coal']
                    self.mix_energ.loc[i, 'Biofuel']=self.iea_results.loc[i, 'Biofuel']
                    self.mix_energ.loc[i, 'Biomass']=self.iea_results.loc[i, 'Biomass']
                    self.mix_energ.loc[i, 'Other']=self.iea_results.loc[i, 'Other']
                else:
                    self.mix_energ.append(self.iea_results.loc[i,:])
                    #self.mix_energ.loc[i, 'Country code']=i
                self.mix_energ=self.mix_energ.fillna(0)
        else:
            # Temporarily fill self.mix_energ with 0 for Biomass and Other sources of energy consumption
            self.mix_energ=self.mix_energ.set_index('Country code')
            for i in self.mix_energ.index:
                self.mix_energ.loc[i,'Biomass']=0
                self.mix_energ.loc[i, 'Other']=0
        
        # Add data from literature to self.mix_energ
        ## For countries with biomass data exclusively, we suppose the mix of other energy sources is the same as the national mix
        ## We suppose that all the countries for which there is no IEA data use the Global biomass use in pulp and paper mills data
        
        # Add biomass fraction to self.mix_energ for countries with no biomass data from IEA
        for p in self.mix_energ.index:
            if p!=0: # because some data in mix_energ are continental, hence don't have a country code, which has been replaced by 0.
                if self.mix_energ.loc[p,'Biomass']==0 and self.mix_energ.loc[p,'Total - EJ']!=0:
                    if self.mix_energ.loc[p, 'Entity'] in self.biom_consumption.index:
                        self.mix_energ.loc[p, 'Biomass'] = self.biom_consumption.loc[self.mix_energ.loc[p,'Entity'], 'Value']
                    else:
                        self.mix_energ.loc[p,'Biomass']= self.biom_consumption.loc['Global', 'Value']
                    self.mix_energ.loc[p, 'Oil']=self.mix_energ.loc[p, 'Oil']*(1-self.mix_energ.loc[p,'Biomass'])
                    self.mix_energ.loc[p, 'Gas']=self.mix_energ.loc[p, 'Gas']*(1-self.mix_energ.loc[p,'Biomass'])
                    self.mix_energ.loc[p, 'Coal']=self.mix_energ.loc[p, 'Coal']*(1-self.mix_energ.loc[p,'Biomass'])
                    self.mix_energ.loc[p, 'Biofuel']=self.mix_energ.loc[p, 'Biofuel']*(1-self.mix_energ.loc[p,'Biomass'])
                    self.mix_energ.loc[p, 'Other']=self.mix_energ.loc[p, 'Other']*(1-self.mix_energ.loc[p,'Biomass'])
                
                elif self.mix_energ.loc[p, 'Biomass']==0 and self.mix_energ.loc[p,'Total - EJ']==0 :
                    # Global average because no available data for country
                    self.mix_energ.loc[p, 'Biomass'] = self.init_data.loc['Global average energy mix - Biomass fraction']
                    self.mix_energ.loc[p, 'Oil']=self.init_data.loc['Global average energy mix - Oil fraction']
                    self.mix_energ.loc[p, 'Gas']=self.init_data.loc['Global average energy mix - Gas fraction']
                    self.mix_energ.loc[p, 'Coal']=self.init_data.loc['Global average energy mix - Coal fraction']
                    self.mix_energ.loc[p, 'Biofuel']=self.init_data.loc['Global average energy mix - Biofuel fraction']
                    self.mix_energ.loc[p, 'Other']=self.init_data.loc['Global average energy mix - Other fraction']
        
        # Change index of mix_energ for country codes
        self.mix_energ= self.mix_energ.groupby(self.mix_energ.index).mean()
        self.mix_energ['Country code']= self.mix_energ.index
        
        # Ecoinvent database importation
        self.database =self.init_data.loc['Ecoinvent database path']

        # Download world map to create choropleth
        self.world_map = json.load(open(self.init_data.loc['World map path']))
        
        # A, F, C matrix importation
        with gzip.open(r'C:\Users\Arianne\Doctorat en génie chimique\Bases de données Ecoinvent\ecoinvent_3-7-1_cutoffPandas_symmNorm.gz.pickle') as f:
            self.eco = pickle.load(f)
            self.imp = self.eco['IMP']
            self.pro = self.eco['PRO']
            self.A = self.eco['A']
            self.F = self.eco['F']
            self.C = self.eco['C']
            self.shortlabel = self.pro[self.meta].reset_index()
            
        self.A_original = self.A.copy(deep = True) # keep a copy of original A matrix
        
        # Add new destination processes to shortlabel
        self.shortlabel_add = pd.DataFrame (index = [x for x in range(1+self.shortlabel.index[-1], self.destination.index.size+1+self.shortlabel.index[-1])], columns = self.shortlabel.columns)
        counter = 1+self.shortlabel.index[-1]
        for p in self.destination.index:
            self.shortlabel_add.loc[counter, 'index']=self.destination.loc[p, 'New process id']
            self.shortlabel_add.loc[counter, 'activityName']='Recycling'
            self.shortlabel_add.loc[counter, 'productName']= self.init_data.loc['Product']
            self.shortlabel_add.loc[counter, 'geography']=p
            self.shortlabel_add.loc[counter, 'unitName']='kg'
            counter = counter+1
        self.shortlabel = self.shortlabel.append(self.shortlabel_add)
            
        # Add new biofuel and sludge incineration processes to shortlabel
        self.shortlabel_add1 = pd.DataFrame (index = [x for x in range(1+self.shortlabel.index[-1], self.new_processes.index.size+1+self.shortlabel.index[-1])], columns = self.shortlabel.columns)
        counter_new = 1+self.shortlabel.index[-1]
        for p in self.new_processes.index:
            self.shortlabel_add1.loc[counter_new, 'index']=self.new_processes.loc[p, 'New process id']
            self.shortlabel_add1.loc[counter_new, 'activityName']=p
            self.shortlabel_add1.loc[counter_new, 'productName']= self.new_processes.loc[p, 'productName']
            self.shortlabel_add1.loc[counter_new, 'geography']=self.new_processes.loc[p,'geography']
            self.shortlabel_add1.loc[counter_new, 'unitName']=self.new_processes.loc[p, 'unitName']
            counter_new = counter_new+1
        self.shortlabel = self.shortlabel.append(self.shortlabel_add1)
        
        print('New processes:')
        display(self.shortlabel_add1)
        display(self.shortlabel_add)
        
        # C matrix definition: self.C_ACV matrix definition (C matrix containing only selected impact categories)
        ## Filter for default impact categories
        filtre0 =self.init_data.loc["Filter for impact categories"] 
        self.impacts = self.imp[self.imp.index.str.contains(filtre0, case=False, regex=False).fillna(False)]
        ## Append supplementary impacts
        self.impacts = self.impacts.append(self.supp_impacts)          
        ## Create new C matrix with only selected impact categories (self.C_ACV)
        self.C_ACV = self.C.loc[self.impacts.index] 
        ## Display self.C_ACV
        #pd.set_option("display.max_rows", None, "display.max_columns", None)
        #display(self.C_ACV.index)  

#___________________________________________________________________________                                   
# CLUSTERING METHODS                                   
#___________________________________________________________________________   

    def proxy_cluster(self, data_to_estimate, column_estimate, data_tech, column_tech):
        """ Estimation of a parameter for all regions based on technological level data (PIB/capita, recycling rates, ...)
        
        Parameters:
        data_to_estimate: Dataframe containing the available data with a column containing the country codes ('Country code') in iso3 format and a column containing values (column_estimate)
        column_estimate: Name of the column containing the available data values in data_to_estimate
        data_tech: Proxy to use to estimate the values for the countries without data. Dataframe containing a column with the country codes ('Country code') and a column with the data used as proxy (column_tech)
        column_tech: Name of the column containing the proxy values in data_tech
        
        Returns:
        self.proxy_data: DataFrame containing the country codes and the estimation of the parameter for every region
        
        *Assumes that data_tech has only one occurence for each region
        
        """

        # Estimation of a parameter for all regions based on technological level data (PIB/capita, recycling rates, ...)
        # data_to_estimate is a dataframe containing the available data with a column containing the country codes ('Country code') and a column containing values (column_estimate)
        # data_tech is the proxy to use to estimate the values for the country without data. Dataframe containing a column with the country codes ('Country code') and a column with the data used as proxy (column_tech)
        # Assumes that data_tech has only one occurence for each region

        # Create dataframe with available data (from data_to_estimate) for each country (if a country appears multiple times, the mean value is calculated)
        data_to_estimate = data_to_estimate.groupby('Country code').mean()
        # Create dataframe with available data (from data_tech) for each country (if a country appears multiple times, the mean efficiency value is calculated)
        data_tech = data_tech.groupby('Country code').mean()
        # Create dataframe with recycling data and index is country codes
        self.recyc_rate.set_index('Country code', inplace=True)
        self.recyc_rate.loc[:,'Country code'] = self.recyc_rate.index

        # Create dataframe with technological data cluster values
        self.cluster_centers = pd.DataFrame(index=data_tech.index.intersection(data_to_estimate.index), columns = ['Data', 'Proxy'])
        for i in self.cluster_centers.index:
            self.cluster_centers.loc[i, 'Data']= data_to_estimate.loc[i, column_estimate]
            self.cluster_centers.loc[i, 'Proxy']= data_tech.loc[i, column_tech]

        # Create dataframe with data to estimate values for each country
        self.proxy_data = pd.DataFrame(index=data_tech.index, columns=['Estimation'])

        # Fill the dataframe with known values
        for i in data_tech.index:
            if i in data_to_estimate.index:
                self.proxy_data.loc[i,'Estimation']=data_to_estimate.loc[i, column_estimate]
            else:
                self.proxy_data.loc[i,'Estimation']='not defined'

        # Approximate other values with proxy data 
        for i in self.proxy_data.index:
            if self.proxy_data.loc[i,'Estimation']=='not defined':
                if self.recyc_rate.loc[i, 'Recycling']!='no':
                    for j in self.cluster_centers.index:
                        self.cluster_centers.loc[j, i] =abs(self.cluster_centers.loc[j, 'Proxy']-data_tech.loc[i, column_tech])
                    if math.isnan(min(self.cluster_centers[i].tolist()))==False:
                        index_min = self.cluster_centers[i].tolist().index(min(self.cluster_centers[i].tolist()))
                        data_estimation = self.cluster_centers['Data'].tolist()[index_min]
                        self.proxy_data.loc[i, 'Estimation']=data_estimation
                # Countries with 0 recycling
                elif self.recyc_rate.loc[i, 'Recycling']=='no':
                    self.proxy_data.loc[i,'Estimation']='no recycling'
        # Add a column 'Country code' to facilitate choropleth generation
        self.proxy_data['Country code']=self.proxy_data.index

        return self.proxy_data

#___________________________________________________________________________   
    def water_cluster(self, water_emissions, data_tech, column_tech):
        """Estimation of the emissions to water for all regions based on technological level data (PIB/capita, recycling rates, ...)
        
        Parameters:
        data_tech: Proxy to use to estimate the values for the countries without data. Dataframe containing a column with the country codes ('Country code') and a column with the data used as proxy (column_tech)    
        column_tech: Name of the column containing the proxy values in data_tech
        
        Returns:
        self.wat_emi_cluster: DataFrame containing the country codes and the estimation of the emissions to water for every region
        
        *Water emissions need a new method for clustering because of many emission categories.
        
        """
    
        self.water_emissions = water_emissions
        self.wat_emi_coun = self.water_emissions.groupby('Country code').mean()
        self.wat_emi_coun = self.wat_emi_coun.fillna('not defined')
        self.wat_emi_cluster = pd.DataFrame(columns = self.wat_emi_coun.columns)
        for j in self.wat_emi_coun.columns:
            self.wat_emi_coun_mod=self.wat_emi_coun
            for i in self.wat_emi_coun.index:
                if self.wat_emi_coun.loc[i, j]=='not defined':
                    self.wat_emi_coun_mod = self.wat_emi_coun_mod.drop(i, axis=0)
            self.proxy_cluster(data_to_estimate=self.wat_emi_coun_mod, column_estimate=j, data_tech=data_tech, column_tech=column_tech)
            self.wat_emi_cluster.loc[:, j] = self.proxy_data.loc[:, 'Estimation']
        self.wat_emi_cluster.loc[:,'Country code']=self.wat_emi_cluster.index
        
        return self.wat_emi_cluster
#___________________________________________________________________________                                   
    def choropleth_generator(self, data, country_code_col, data_col, color_col):
        """ Generates a choropleth with raw data or clustered data
        
        Parameters:
        data: Dataframe containing country codes and cluster results in 2 different columns (country_code_col and color_col)
        country_code_col: Name of the column containing country codes in data
        data_col: Name of the column with the data that has been clustered
        color_col: Name of the column containing the data to create the choropleth (often the same as data_col)
        
        """
        # Remove strings in data column, and convert to float rounded to 4

        for i in data.index:
            if isinstance(data.loc[i,data_col],str):
                data = data.drop(i)

        data[data_col] = data[data_col].astype(float).round(4)

        # Mean with duplicated countries
        if data[country_code_col].duplicated().sum()!=0:
            # Create dataframe with available data (from data_to_estimate) for each country (if a country appears multiple times, the mean efficiency value is calculated)
            data.index.names = ['Index']
            data = data.groupby('Country code').mean()
            data['Country code']= data.index
        
        data.index.names = ['Index']

        # Create a dictionary with region ids and region names
        region_id='iso_a3' # always use iso-a3 norm for country ids
        country_id_map={}
        for feature in self.world_map['features']:
            country_id_map[feature['properties'][region_id]]=feature['properties']['name']

        # Associate regionalized clustered data with region id in world map
        for d in data.index:
            if data.loc[d,country_code_col] in country_id_map:
                data.loc[d, 'id']=data.loc[d, country_code_col]
            else:
                data.loc[d,'id']='NaN'

        # Create choropleth map
        self.choropleth=px.choropleth(data, locations='id', locationmode='ISO-3',geojson=self.world_map,color=color_col,hover_name=country_code_col,hover_data=[data_col], color_discrete_sequence=['navy','darkblue','deepskyblue','limegreen','gold','darkorange','red'], color_continuous_scale=['deepskyblue','gold','red'], width=2000, height=1600)
        self.choropleth.show()
        
#___________________________________________________________________________                                 
# NEW GEOGRAPHICALLY DEFINED PROCESSES CREATION METHODS
#___________________________________________________________________________          
    def _change_efficiency(self):
        """Efficiency adjustment method: combines efficiency adjustment, MSW and sludge generation, MSW and sludge treatments, and transport   
        - Change efficiencies to geographically defined efficiencies to new processes in self.A       
        
        *Must run proxy_cluster with efficiency data before using this method
        
        """
    # Efficiency adjustment method: combines efficiency adjustment, MSW and sludge generation, MSW and sludge treatments, and transport   
    # Change efficiencies to geographically defined efficiencies in new processes in self.A       
    # ***Must run proxy_cluster with efficiency data prior to using this function
    
    #_________________________________________________
    #Adjust input
        
        # Create a new column with efficiencies in self.destination 
        for p in self.destination.index:
            if (self.destination.loc[p,'Country code'] in self.proxy_data.index) and ((self.proxy_data.loc[self.destination.loc[p,'Country code'],'Estimation'])!='not defined') and ((self.proxy_data.loc[self.destination.loc[p,'Country code'],'Estimation'])!='no recycling'):
                self.destination.loc[p, 'Efficiency'] = self.proxy_data.loc[self.destination.loc[p,'Country code'], 'Estimation']
            else:
                display('No efficiency data available for region: '+p+'. Provide proxy data for this region or select another similar region')
                exit()
    
        # Determine total input mass (virgin + secondary material without impurities) and create a new column in self.destination
        for p in self.destination.index:
            self.destination.loc[p, 'Input mass']=1/self.destination.loc[p,'Efficiency']
    
        # Create new columns with total input quantities for each virgin input in self.destination and secondary input
        for p in self.destination.index:
            # Virgin input
            self.destination.loc[p, 'Total sulfate pulp, bleached input (kg)'] = self.destination.loc[p, 'Ratio of sulfate pulp, bleached (kg): total input (kg)']*self.destination.loc[p, 'Input mass']
            self.destination.loc[p,'Total sulfate pulp, unbleached input (kg)'] = self.destination.loc[p, 'Ratio of sulfate pulp, unbleached (kg) : total input (kg)']*self.destination.loc[p, 'Input mass']
            self.destination.loc[p, 'Total wood input (m3)'] = self.destination.loc[p, 'Ratio of wood (m3) : total input (kg)']*self.destination.loc[p, 'Input mass']
            # Secondary product input without impurities (suppose softwood has a density of 522,5 kg/m3)
            self.destination.loc[p, 'Total secondary input (kg)']=self.destination.loc[p, 'Input mass']-self.destination.loc[p, 'Total sulfate pulp, bleached input (kg)']-self.destination.loc[p,'Total sulfate pulp, unbleached input (kg)']-(self.destination.loc[p, 'Total wood input (m3)'])*522.5
    
        # Compute total MSW (without sludge): assume all impurities in initial bale result in MSW and create new column in self.destination containing total MSW produced by each process
        fraction_recycled = 0
        fraction_not_recycled = 0
        for m in self.composition.index:
            if self.composition.loc[m, 'Recycled']=='yes':
                fraction_recycled+=self.composition.loc[m, 'Fraction']
            elif self.composition.loc[m,'Recycled']=='no':
                fraction_not_recycled+=self.composition.loc[m, 'Fraction']
        for p in self.destination.index:    
            self.destination.loc[p, 'Total MSW'] = fraction_not_recycled*self.destination.loc[p, 'Total secondary input (kg)']/fraction_recycled 
                
            
        #Modify total input in A
        for p in self.destination.index:
            # Virgin input
            self.A= self._sp_loc(df = self.A, index =self.default_keys.loc['Sulfate pulp, bleached, production', 'Key'], columns = self.destination.loc[p, 'New process id'], val = self.destination.loc[p, 'Total sulfate pulp, bleached input (kg)'])
            self.A= self._sp_loc(df = self.A, index = self.default_keys.loc['Sulfate pulp, unbleached, production', 'Key'], columns = self.destination.loc[p, 'New process id'], val = self.destination.loc[p,'Total sulfate pulp, unbleached input (kg)'])
            self.A= self._sp_loc(df = self.A, index = self.default_keys.loc['Wood production', 'Key'], columns = self.destination.loc[p, 'New process id'], val = self.destination.loc[p, 'Total wood input (m3)'])
            # Secondary product input (with impurities)
            self.A = self._sp_loc(df=self.A, index = self.rawmat_key, columns = self.destination.loc[p, 'New process id'], val=self.destination.loc[p, 'Total secondary input (kg)']+self.destination.loc[p,'Total MSW'])  
    #_________________________________________________
    #Adjust MSW generation and treatment
    
        for p in self.destination.index:
            total_MSW_treatment = self.waste_treatment.loc[self.destination.loc[p, 'Country code'], 'Open dump']+self.waste_treatment.loc[self.destination.loc[p, 'Country code'], 'Landfill unspecified']+self.waste_treatment.loc[self.destination.loc[p, 'Country code'], 'Controlled landfill']+self.waste_treatment.loc[self.destination.loc[p, 'Country code'], 'Sanitary landfill']+self.waste_treatment.loc[self.destination.loc[p, 'Country code'], 'Incineration']
            if total_MSW_treatment ==0 and self.init_data.loc['Product']=='Corrugated cardboard':
                # No MSW treatment available for country so RoW municipal waste treatment process is used.We add MSW from efficiency losses for Corrugated cardboard production process because nos sludge is produced.
                MSW_cardboard = ((self.destination.loc[p, 'Input mass'])*(1-self.init_data.loc['Water content of wastepaper (without impurities)'])-1*(1-self.init_data.loc['Water content of recycled paper product']))/(1-self.init_data.loc['Water content of rejected fibres (containerboard production)'])
                self.A = self._sp_loc(df=self.A, index = self.default_keys.loc['MSW treatment', 'Key'], columns = self.destination.loc[p,'New process id'], val =-MSW_cardboard - self.destination.loc[p,'Total MSW'] )
            elif total_MSW_treatment ==0 and self.init_data.loc['Product']!='Corrugated cardboard':
                # No MSW treatment available for country so RoW municipal waste treatment process is used.
                self.A = self._sp_loc(df=self.A, index = self.default_keys.loc['MSW treatment', 'Key'], columns = self.destination.loc[p,'New process id'], val =- self.destination.loc[p,'Total MSW'] )
            else:    
                # Fraction of each MSW treatment type for each country
                fraction_MSW_open_dump = (self.waste_treatment.loc[self.destination.loc[p, 'Country code'], 'Open dump'])
                fraction_MSW_landfill =  self.waste_treatment.loc[self.destination.loc[p, 'Country code'], 'Landfill unspecified']+self.waste_treatment.loc[self.destination.loc[p, 'Country code'], 'Controlled landfill']+self.waste_treatment.loc[self.destination.loc[p, 'Country code'], 'Sanitary landfill']
                fraction_MSW_incineration = self.waste_treatment.loc[self.destination.loc[p, 'Country code'], 'Incineration']

                for m in self.composition.index:
                    if ((self.composition.loc[m,'Recycled']=='no') and (self.init_data.loc['Product']!='Corrugated cardboard'))|((self.composition.loc[m,'Recycled']=='no') and (self.init_data.loc['Product']=='Corrugated cardboard') and (m!='Other')):
                        #landfill
                        if fraction_MSW_landfill!=0:
                            self.A = self._sp_loc(df=self.A, index = self.waste_keys.loc[m, 'Landfill'], columns = self.destination.loc[p,'New process id'], val = -self.composition.loc[m,'Fraction']*self.destination.loc[p, 'Total secondary input (kg)']/fraction_recycled*fraction_MSW_landfill/total_MSW_treatment)
                            #print(p,m)
                            #print('Landfill:', -self.composition.loc[m,'Fraction']*self.destination.loc[p, 'Total secondary input (kg)']/fraction_recycled*fraction_MSW_landfill/total_MSW_treatment)
                        # incineration
                        if fraction_MSW_incineration!=0:
                            self.A = self._sp_loc(df=self.A, index = self.waste_keys.loc[m, 'Incineration'], columns = self.destination.loc[p,'New process id'], val = -self.composition.loc[m,'Fraction']*self.destination.loc[p, 'Total secondary input (kg)']/fraction_recycled*fraction_MSW_incineration/total_MSW_treatment)
                            #print(p,m)
                            #print('Incineration', -self.composition.loc[m,'Fraction']*self.destination.loc[p, 'Total secondary input (kg)']/fraction_recycled*fraction_MSW_incineration/total_MSW_treatment)
                        # open dump
                        if fraction_MSW_open_dump!=0:
                            key = self.open_dump_keys.loc[self.infiltration.loc[self.destination.loc[p, 'Country code 2'], 'Infiltration Class'], m]
                            self.A = self._sp_loc(df = self.A, index = key, columns = self.destination.loc[p,'New process id'], val = -self.composition.loc[m,'Fraction']*self.destination.loc[p, 'Total secondary input (kg)']/fraction_recycled*fraction_MSW_open_dump/total_MSW_treatment)
                            #print(p,m)
                            #print('Open dump',-self.composition.loc[m,'Fraction']*self.destination.loc[p, 'Total secondary input (kg)']/fraction_recycled*fraction_MSW_open_dump/total_MSW_treatment)
                    
                    # Add MSW from efficiency if product is Corrugated cardboard
                    elif (self.composition.loc[m,'Recycled']=='no') and (m=='Other') and self.init_data.loc['Product']=='Corrugated cardboard':
                        MSW_cardboard = ((self.destination.loc[p, 'Input mass'])*(1-self.init_data.loc['Water content of wastepaper (without impurities)'])-1*(1-self.init_data.loc['Water content of recycled paper product']))/(1-self.init_data.loc['Water content of rejected fibres (containerboard production)'])
                        #landfill
                        if fraction_MSW_landfill!=0:
                            self.A = self._sp_loc(df=self.A, index = self.waste_keys.loc[m, 'Landfill'], columns = self.destination.loc[p,'New process id'], val = -(MSW_cardboard+self.composition.loc[m,'Fraction']*self.destination.loc[p, 'Total secondary input (kg)']/fraction_recycled)*fraction_MSW_landfill/total_MSW_treatment)
                            #print(p,m)
                            #print('Landfill', -(MSW_cardboard+self.composition.loc[m,'Fraction']*self.destination.loc[p, 'Total secondary input (kg)']/fraction_recycled)*fraction_MSW_landfill/total_MSW_treatment)
                        # incineration
                        if fraction_MSW_incineration !=0:
                            self.A = self._sp_loc(df=self.A, index = self.waste_keys.loc[m, 'Incineration'], columns = self.destination.loc[p,'New process id'], val = -(MSW_cardboard+self.composition.loc[m,'Fraction']*self.destination.loc[p, 'Total secondary input (kg)']/fraction_recycled)*fraction_MSW_incineration/total_MSW_treatment)
                            #print(p,m)
                            #print('Incineration', -(MSW_cardboard+self.composition.loc[m,'Fraction']*self.destination.loc[p, 'Total secondary input (kg)']/fraction_recycled)*fraction_MSW_incineration/total_MSW_treatment)
                        # open dump
                        if fraction_MSW_open_dump!=0:
                            key = self.open_dump_keys.loc[self.infiltration.loc[self.destination.loc[p, 'Country code 2'], 'Infiltration Class'],m]
                            self.A = self._sp_loc(df = self.A, index = key, columns = self.destination.loc[p,'New process id'], val = -(MSW_cardboard+self.composition.loc[m,'Fraction']*self.destination.loc[p, 'Total secondary input (kg)']/fraction_recycled)*fraction_MSW_open_dump/total_MSW_treatment)
                            #print(p,m)
                            #print('Open dump', -(MSW_cardboard+self.composition.loc[m,'Fraction']*self.destination.loc[p, 'Total secondary input (kg)']/fraction_recycled)*fraction_MSW_open_dump/total_MSW_treatment)
    
        self.fraction_recycled = fraction_recycled 
    #_________________________________________________
     #Add transport
        for p in self.destination.index:
            input_total_mass = self.destination.loc[p, 'Input mass']/fraction_recycled
            transport_key = self.default_keys.loc[self.destination.loc[p,'Main transport'], 'Key']
            self.A = self._sp_loc(df=self.A, index = transport_key, columns = self.destination.loc[p, 'New process id'], val=input_total_mass/1000*self.destination.loc[p,'Distance from exporting country (km)'])
    #_________________________________________________
     #Adjust sludge generation and treatments
        
        # Compute total sludge production and create a new column in self.destination
        # Assume sludge is composed of input fibers and water only (chemicals are added after deinking)
        
        if self.init_data.loc['Product']!= 'Corrugated cardboard':
            for p in self.destination.index:
                self.destination.loc[p, 'Total sludge']=((self.destination.loc[p, 'Input mass'])*(1-self.init_data.loc['Water content of wastepaper (without impurities)'])-1*(1-self.init_data.loc['Water content of recycled paper product']))/(1-self.init_data.loc['Sludge water content'])

            # Determine treatment processes for sludge for every destination
            sludge_incineration_key = self.new_processes.loc['Sludge incineration', 'New process id']
            sludge_landfill_key = self.default_keys.loc['Sludge landfill', 'Key']
            sludge_landfarming_key = self.default_keys.loc['Sludge landfarming', 'Key']
            sludge_default = self.default_keys.loc['Sludge treatment', 'Key']

            for p in self.destination.index: 
                if (self.sludge_treatments[self.sludge_treatments['Country code'].str.contains(self.destination.loc[p,'Country code']).fillna(False)]).shape[0]!=0:
                    # Use known data from literature
                    self.sludge_inc = self.sludge_treatments.set_index('Country code').loc[self.destination.loc[p, 'Country code'],'Incineration']
                    self.sludge_landfill = self.sludge_treatments.set_index('Country code').loc[self.destination.loc[p, 'Country code'],'Landfill']
                    self.sludge_landfarm = self.sludge_treatments.set_index('Country code').loc[self.destination.loc[p, 'Country code'],'Landfarming']

                    # self.A matrix reconstruction for sludge treatment
                    self.A = self._sp_loc(df=self.A, index = sludge_incineration_key, columns = self.destination.loc[p, 'New process id'], val=-self.destination.loc[p, 'Total sludge']*self.sludge_inc)
                    self.A = self._sp_loc(df=self.A, index = sludge_landfill_key, columns = self.destination.loc[p, 'New process id'], val=-self.destination.loc[p, 'Total sludge']*self.sludge_landfill)
                    self.A = self._sp_loc(df=self.A, index = sludge_landfarming_key, columns = self.destination.loc[p, 'New process id'], val=-self.destination.loc[p, 'Total sludge']*self.sludge_landfarm)
                    # Assuming no impact and no substitution attributed to sludge used as filler
            
                # Use MSW national treatment data from World Bank report
                elif self.destination.loc[p,'Country code'] in self.waste_treatment_sludge.index and (self.sludge_treatments[self.sludge_treatments['Country code'].str.contains(self.destination.loc[p,'Country code']).fillna(False)]).shape[0]==0:
                    v = self.destination.loc[p,'Country code']
                    # Take data from waste treatment
                    sludge_inc = self.waste_treatment_sludge.loc[v, 'Incineration']/100
                    sludge_landfill = 1/100*(self.waste_treatment_sludge.loc[v, 'Open dump']+self.waste_treatment_sludge.loc[v, 'Landfill unspecified']+self.waste_treatment_sludge.loc[v, 'Controlled landfill']+self.waste_treatment_sludge.loc[v, 'Sanitary landfill'])
                    sludge_landfarm = self.waste_treatment_sludge.loc[v,'Composting']/100
                    sludge_tot = sludge_inc+sludge_landfill+sludge_landfarm
                    if sludge_tot !=0:
                        self.sludge_inc = sludge_inc/sludge_tot
                        self.sludge_landfill = sludge_landfill/sludge_tot
                        self.sludge_landfarm=sludge_landfarm/sludge_tot

                        # self.A matrix reconstruction for sludge treatment
                        self.A = self._sp_loc(df=self.A, index = sludge_incineration_key, columns = self.destination.loc[p, 'New process id'], val=-self.destination.loc[p, 'Total sludge']*self.sludge_inc)
                        self.A = self._sp_loc(df=self.A, index = sludge_landfill_key, columns = self.destination.loc[p, 'New process id'], val=-self.destination.loc[p, 'Total sludge']*self.sludge_landfill)
                        self.A = self._sp_loc(df=self.A, index = sludge_landfarming_key, columns = self.destination.loc[p, 'New process id'], val=-self.destination.loc[p, 'Total sludge']*self.sludge_landfarm)
   
                        ## Substitution of fertiliser for landfarmed sludge
                        self.A = self._sp_loc(df=self.A, index = self.default_keys.loc['N organo-mineral fertiliser', 'Key'], columns = self.destination.loc[p, 'New process id'], val=-self.destination.loc[p, 'Total sludge']*sludge_landfarm*self.init_data.loc['N organo-mineral fertiliser from sludge landfarming (kg/kg sludge)'])
                        self.A = self._sp_loc(df=self.A, index = self.default_keys.loc['P2O5 organo-mineral fertiliser', 'Key'], columns = self.destination.loc[p, 'New process id'], val=-self.destination.loc[p, 'Total sludge']*sludge_landfarm*self.init_data.loc['P2O5 organo-mineral fertiliser from sludge landfarming (kg/kg sludge)'])
                        self.A = self._sp_loc(df=self.A, index = self.default_keys.loc['K2O organo-mineral fertiliser', 'Key'], columns = self.destination.loc[p, 'New process id'], val=-self.destination.loc[p, 'Total sludge']*sludge_landfarm*self.init_data.loc['K2O organo-mineral fertiliser from sludge landfarming (kg/kg sludge)'])
                        self.A = self._sp_loc(df=self.A, index = self.default_keys.loc['CaCO3 pH raising agent', 'Key'], columns = self.destination.loc[p, 'New process id'], val=-self.destination.loc[p, 'Total sludge']*sludge_landfarm*self.init_data.loc['CaCO3 pH raising agent from sludge landfarming (kg/kg sludge)'])

                        # Assuming no impact and no substitution attributed to sludge used as filler
                    else:
                        # Default RoW market without substitution
                        self.A = self._sp_loc(df = self.A, index=self.default_keys.loc['Sludge treatment', 'Key'], columns =self.destination.loc[p, 'New process id'], val = -self.destination.loc[p,'Total sludge'])
                        self.sludge_inc = 0
                
                else:
                    # Default RoW market without substitution
                    self.A = self._sp_loc(df=self.A, index = sludge_default, columns = self.destination.loc[p, 'New process id'], val=-self.destination.loc[p, 'Total sludge'])    
                    self.sludge_inc = 0
                
                self.destination.loc[p,'Sludge incinerated fraction']=self.sludge_inc
 
        else:
            for p in self.destination.index:
                self.destination.loc[p, 'Total sludge']=0
                self.destination.loc[p,'Sludge incinerated fraction']=0
                
        self.elec_prod_sludge = self.init_data.loc['Electricity production from sludge incineration (kWh/kg)']
        self.energy_prod_sludge = self.init_data.loc['Energy production from sludge incineration (MJ/kg)']
    #___________________________________________________________________________        
    def _change_mix(self):
        """Changes electricity mixes to geographically defined electricity mixes to new processes in self.A
        
        * User must run change_efficiency method before using this method
        * User must run proxy_cluster method with electricity consumption before using this method
        
        """
    
        for p in self.destination.index:
            if (self.destination.loc[p,'Country code'] in self.proxy_data.index) and ((self.proxy_data.loc[self.destination.loc[p,'Country code'],'Estimation'])!='not defined') and ((self.proxy_data.loc[self.destination.loc[p,'Country code'],'Estimation'])!='no recycling'):
                # Create a new column with electricity consumption in self.destination
                self.destination.loc[p, 'Electricity consumption'] =  self.proxy_data.loc[self.destination.loc[p,'Country code'], 'Estimation']-self.destination.loc[p,'Sludge incinerated fraction']*self.elec_prod_sludge*self.destination.loc[p,'Total sludge']
            else:
                display('No electricity data available for region: '+p+'. Verify if there is recycling in this region, provide proxy data for this region or select another similar region')
                exit()
            # Determine replacement processes for every geography
            #print(p)
            filtre1 = self.pro.geography==self.geographies.loc[p, 'shortname']
            filtre2 = self.pro.productName=='electricity, medium voltage'
            filtre3=self.pro.activityName=='market for electricity, medium voltage'
            filtre4=self.pro.activityName=='market group for electricity, medium voltage'
            elec_poss = self.pro.loc[(filtre1 & filtre2 & (filtre3 | filtre4))][self.meta]
            if elec_poss.shape[0]==0:
                #print('No process correspond to your criterias for medium voltage. Here are other options for chosen region:')
                filtre2 = self.pro.productName=='electricity, high voltage'
                filtre3=self.pro.activityName=='market for electricity, high voltage'
                filtre4=self.pro.activityName=='market group for electricity, high voltage'
                elec_poss = self.pro.loc[(filtre1 & filtre2 & (filtre3 | filtre4))][self.meta]
                if elec_poss.shape[0]==0:
                    #print('No process correspond to your criterias for high voltage. Here are other options for chosen region:')
                    filtre2 = self.pro.productName=='electricity, low voltage'
                    filtre3=self.pro.activityName=='market for electricity, low voltage'
                    filtre4=self.pro.activityName=='market group for electricity, low voltage'
                    elec_poss = self.pro.loc[(filtre1 & filtre2 & (filtre3 | filtre4))][self.meta]
                    if elec_poss.shape[0]==0:
                        mix_sub_key = self.default_keys.loc['Electricity mix', 'Key']
                    else: 
                        #display(elec_poss)
                        mix_sub_key = elec_poss.index[0]  
                else:
                    #display(elec_poss)
                    mix_sub_key = elec_poss.index[0]
            else:
                #display(elec_poss)
                mix_sub_key = elec_poss.index[0]
                #display(mix_sub_key)
            self.A = self._sp_loc(self.A, mix_sub_key, self.destination.loc[p, 'New process id'], self.destination.loc[p, 'Electricity consumption'] )
#___________________________________________________________________________        
    def _change_energ(self):
        """Changes energy mixes to geographically defined energy mixes to new processes in self.A
        
        * User must run change_efficiency method before using this method
        * User must run proxy_cluster method with energy consumption and biomass_cluster method before using this method
        
        """
        for p in self.destination.index:
            if (self.destination.loc[p,'Country code'] in self.proxy_data.index) and ((self.proxy_data.loc[self.destination.loc[p,'Country code'],'Estimation'])!='not defined') and ((self.proxy_data.loc[self.destination.loc[p,'Country code'],'Estimation'])!='no recycling') and self.destination.loc[p,'Country code']in self.mix_energ.index:
                # Create a new column with energy consumption in self.destination (value is in GJ)
                self.destination.loc[p, 'Energy consumption'] =  self.proxy_data.loc[self.destination.loc[p,'Country code'], 'Estimation']-self.destination.loc[p,'Sludge incinerated fraction']*self.energy_prod_sludge*self.destination.loc[p,'Total sludge']/1000 # Total energy consumption is in GJ/kg
            elif (self.destination.loc[p,'Country code'] in self.proxy_data.index) and ((self.proxy_data.loc[self.destination.loc[p,'Country code'],'Estimation'])!='not defined') and ((self.proxy_data.loc[self.destination.loc[p,'Country code'],'Estimation'])!='no recycling') and not self.destination.loc[p,'Country code']in self.mix_energ.index:
                self.destination.loc[p, 'Energy consumption'] =  self.proxy_data.loc[self.destination.loc[p,'Country code'], 'Estimation']-self.destination.loc[p,'Sludge incinerated fraction']*self.energy_prod_sludge*self.destination.loc[p,'Total sludge']/1000 # Total energy consumption is in GJ/kg
                # Global average because no available data for country
                self.mix_energ.loc[self.destination.loc[p,'Country code'], 'Biomass'] = self.init_data.loc['Global average energy mix - Biomass fraction']
                self.mix_energ.loc[self.destination.loc[p,'Country code'], 'Oil']=self.init_data.loc['Global average energy mix - Oil fraction']
                self.mix_energ.loc[self.destination.loc[p,'Country code'], 'Gas']=self.init_data.loc['Global average energy mix - Gas fraction']
                self.mix_energ.loc[self.destination.loc[p,'Country code'], 'Coal']=self.init_data.loc['Global average energy mix - Coal fraction']
                self.mix_energ.loc[self.destination.loc[p,'Country code'], 'Biofuel']=self.init_data.loc['Global average energy mix - Biofuel fraction']
                self.mix_energ.loc[self.destination.loc[p,'Country code'], 'Other']=self.init_data.loc['Global average energy mix - Other fraction']
            else:
                display('No energy consumption data available for region: '+p+'. Verify if there is recycling in this region, provide proxy data for this region or select another similar region')
                exit()
            
            # Create new columns with energy consumption in Ecoinvent units from each source in self.destination
            ## Mass or volume units (for production processes) (Energy consumption is in GJ/kg in template)
            self.destination.loc[p, 'Oil consumption, kg']= self.destination.loc[p,'Energy consumption']*(self.mix_energ.loc[self.destination.loc[p, 'Country code'], 'Oil']+self.mix_energ.loc[self.destination.loc[p, 'Country code'], 'Other'])/42.686*1000 # Heating value of light fuel oil (43,5 MJ/kg) http://www.fao.org/3/T0269e/t0269e0c.htm
            self.destination.loc[p, 'Gas consumption, m3']= self.destination.loc[p,'Energy consumption']*self.mix_energ.loc[self.destination.loc[p, 'Country code'], 'Gas']/36.62*1000 # Heating value of natural gas (36,62 MJ/m^3) http://www.fao.org/3/T0269e/t0269e0c.htm
            self.destination.loc[p, 'Coal consumption, kg']= self.destination.loc[p,'Energy consumption']*self.mix_energ.loc[self.destination.loc[p, 'Country code'], 'Coal']/22.732*1000 # Heating value of hard coal (22,732 MJ/kg) http://www.fao.org/3/T0269e/t0269e0c.htm
            self.destination.loc[p, 'Biofuel consumption, kg']= self.destination.loc[p,'Energy consumption']*self.mix_energ.loc[self.destination.loc[p, 'Country code'], 'Biofuel']/26.952*1000 # Heating value of ethanol (26,952 MJ/kg) https://chemeng.queensu.ca/courses/CHEE332/files/ethanol_heating-values.pdf
            self.destination.loc[p, 'Biomass consumption, kg']= self.destination.loc[p,'Energy consumption']*self.mix_energ.loc[self.destination.loc[p, 'Country code'], 'Biomass']/15.402*1000 # Heating value of wood (15,402 MJ/kg) http://www.fao.org/3/T0269e/t0269e0c.htm
            ## Energy units (for consumption processes)
            self.destination.loc[p, 'Oil consumption, MJ']= self.destination.loc[p,'Energy consumption']*(self.mix_energ.loc[self.destination.loc[p, 'Country code'], 'Oil']+self.mix_energ.loc[self.destination.loc[p, 'Country code'], 'Other'])*1000 # transform GJ to MJ
            self.destination.loc[p, 'Gas consumption, MJ']= self.destination.loc[p,'Energy consumption']*self.mix_energ.loc[self.destination.loc[p, 'Country code'], 'Gas']*1000 # transform GJ to MJ
            self.destination.loc[p, 'Coal consumption, MJ']= self.destination.loc[p,'Energy consumption']*self.mix_energ.loc[self.destination.loc[p, 'Country code'], 'Coal']*1000 # transform GJ to MJ
            self.destination.loc[p, 'Biofuel consumption, MJ']= self.destination.loc[p,'Energy consumption']*self.mix_energ.loc[self.destination.loc[p, 'Country code'], 'Biofuel']*1000 # transform GJ to MJ
            self.destination.loc[p, 'Biomass consumption, MJ']= self.destination.loc[p,'Energy consumption']*self.mix_energ.loc[self.destination.loc[p, 'Country code'], 'Biomass']*1000 # transform GJ to MJ
            
            # self.A matrix reconstruction for energy production
            self.A = self._sp_loc(df=self.A, index = self.default_keys.loc['Oil production', 'Key'], columns = self.destination.loc[p, 'New process id'], val=self.destination.loc[p, 'Oil consumption, kg'])
            self.A = self._sp_loc(df=self.A, index = self.default_keys.loc['Natural gas production', 'Key'], columns = self.destination.loc[p, 'New process id'], val=self.destination.loc[p, 'Gas consumption, m3'])
            self.A = self._sp_loc(df=self.A, index = self.default_keys.loc['Coal production', 'Key'], columns = self.destination.loc[p, 'New process id'], val=self.destination.loc[p, 'Coal consumption, kg'])
            self.A = self._sp_loc(df=self.A, index = self.default_keys.loc['Biofuel production', 'Key'], columns = self.destination.loc[p, 'New process id'], val=self.destination.loc[p, 'Biofuel consumption, kg'])
            self.A = self._sp_loc(df=self.A, index = self.default_keys.loc['Biomass production', 'Key'], columns = self.destination.loc[p, 'New process id'], val=self.destination.loc[p, 'Biomass consumption, kg'])

             # self.A matrix reconstruction for energy consumption
            self.A = self._sp_loc(df=self.A, index = self.default_keys.loc['Oil combustion', 'Key'], columns = self.destination.loc[p, 'New process id'], val=self.destination.loc[p, 'Oil consumption, MJ'])
            self.A = self._sp_loc(df=self.A, index = self.default_keys.loc['Natural gas combustion', 'Key'], columns = self.destination.loc[p, 'New process id'], val=self.destination.loc[p, 'Gas consumption, MJ'])
            self.A = self._sp_loc(df=self.A, index = self.default_keys.loc['Coal combustion', 'Key'], columns = self.destination.loc[p, 'New process id'], val=self.destination.loc[p, 'Coal consumption, MJ'])
            self.A = self._sp_loc(df=self.A, index = self.new_processes.loc['Biofuel combustion', 'New process id'], columns = self.destination.loc[p, 'New process id'], val=self.destination.loc[p, 'Biofuel consumption, MJ'])
            self.A = self._sp_loc(df=self.A, index = self.default_keys.loc['Biomass combustion', 'Key'], columns = self.destination.loc[p, 'New process id'], val=self.destination.loc[p, 'Biomass consumption, MJ'])
#___________________________________________________________________________        
    def _change_water(self):
        """Adds water use, water discharge and water emissions to new processes in self.A
        
        * User must run proxy_cluster method with water consumption before using this method
        * User must run change_efficiency before using this method
        * User must run water_cluster before using this method
        
        """
    
        for p in self.destination.index:
            if (self.destination.loc[p,'Country code'] in self.proxy_data.index) and ((self.proxy_data.loc[self.destination.loc[p,'Country code'],'Estimation'])!='not defined') and ((self.proxy_data.loc[self.destination.loc[p,'Country code'],'Estimation'])!='no recycling'):
                # Add new columns with water input to self.destination
                self.destination.loc[p, 'Water input (m3)']= self.proxy_data.loc[self.destination.loc[p,'Country code'], 'Estimation']
            else:
                display('No water consumption data available for region: '+p+'. Provide proxy data for this region or select another similar region')
                exit()
                
            # Add water input to self.F
            self.F = self._sp_loc(df = self.F, index =self.default_keys.loc['Water input', 'Key'], columns = self.destination.loc[p, 'New process id'], val = self.proxy_data.loc[self.destination.loc[p,'Country code'], 'Estimation'] )
            # Compute water discharge with mass balance
            if self.init_data.loc['Product'] == 'Corrugated cardboard':
                 self.water_discharge = self.destination.loc[p, 'Water input (m3)']+self.destination.loc[p, 'Input mass']/1000*self.init_data.loc['Water content of wastepaper (without impurities)']-1/1000*self.init_data.loc['Water content of recycled paper product']-((self.destination.loc[p, 'Input mass'])*(1-self.init_data.loc['Water content of wastepaper (without impurities)'])-1*(1-self.init_data.loc['Water content of recycled paper product']))/(1-self.init_data.loc['Water content of rejected fibres (containerboard production)'])*self.init_data.loc['Water content of rejected fibres (containerboard production)']
            else:
                self.water_discharge = self.destination.loc[p, 'Water input (m3)']+self.destination.loc[p, 'Input mass']/1000*self.init_data.loc['Water content of wastepaper (without impurities)']-1/1000*self.init_data.loc['Water content of recycled paper product']-self.destination.loc[p,'Total sludge']/1000*self.init_data.loc['Sludge water content']
            self.water_dis_air = self.init_data.loc['Percentage of output water evaporated to air']*self.water_discharge
            self.water_dis_water = self.water_discharge-self.water_dis_air
            # Add water discharge to self.F
            # Add water input to self.F
            self.F = self._sp_loc(df = self.F, index =self.default_keys.loc['Water discharge to water', 'Key'], columns = self.destination.loc[p, 'New process id'], val = self.water_dis_water) 
            self.F = self._sp_loc(df = self.F, index =self.default_keys.loc['Water evaporation to air', 'Key'], columns = self.destination.loc[p, 'New process id'], val = self.water_dis_air)
            #print(self.water_discharge, self.water_dis_air)
            # Add water emissions to self.F
            for j in self.wat_emi_cluster.drop('Country code',axis=1).columns:
                if self.F.loc[self.default_keys.loc[j,'Key'], self.destination.loc[p, 'New process id']]< self.wat_emi_cluster.loc[self.destination.loc[p, 'Country code'], j]:
                    self.F = self._sp_loc(df = self.F, index =self.default_keys.loc[j,'Key'], columns = self.destination.loc[p, 'New process id'], val = self.wat_emi_cluster.loc[self.destination.loc[p, 'Country code'], j])
#___________________________________________________________________________        
    def _change_chemicals(self):
        """Changes chemicals quantity to geographically defined chemicals quantities in new processes in self.A                            
        
        * User must run proxy_cluster method with chemicals consumption before using this method
        
        """
        
        self._inspect()
        chem_keys = pd.DataFrame(columns = self.pro_inspect.columns)
        chem_keys_list =pd.DataFrame(self.chemicals_keys.loc[:, self.init_data.loc['Product']])
        chem_keys_list=chem_keys_list.dropna(how='all', axis=0)  
        chem_keys_list.set_index(self.init_data.loc['Product'], inplace=True)
        for i in chem_keys_list.index:
            chem_keys = chem_keys.append(self.pro_inspect.loc[i,:])
        for p in self.destination.index:
            if (self.destination.loc[p,'Country code'] in self.proxy_data.index) and ((self.proxy_data.loc[self.destination.loc[p,'Country code'],'Estimation'])!='not defined') and ((self.proxy_data.loc[self.destination.loc[p,'Country code'],'Estimation'])!='no recycling'):
                # Create a new column with chemicals consumption in self.destination
                self.destination.loc[p, 'Chemicals consumption'] =  self.proxy_data.loc[self.destination.loc[p,'Country code'], 'Estimation']
            else:
                display('No chemicals data available for region: '+p+'. Verify if there is recycling in this region, provide proxy data for this region or select another similar region')
                exit()
            ## Compute total chemicals consumption in base process
            total_chem = 0
            for m in chem_keys.index:
                total_chem+=self.A.loc[m, self.destination.loc[p, 'New process id']]
            ## Compute ratios of each chemical source in base process
            for m in chem_keys.index:
                chem_keys.loc[m, 'Ratio'] = self.A.loc[m, self.destination.loc[p, 'New process id']]/total_chem
            for m in chem_keys.index:
                self.A = self._sp_loc(self.A, m, self.destination.loc[p, 'New process id'], self.destination.loc[p, 'Chemicals consumption']*chem_keys.loc[m, 'Ratio'] )
#___________________________________________________________________________                
    def _regionalize_A_F(self):
        """Adds new regionalized processes and new biofuel combustion and sludge incineration processes to A and F matrix
        - Creates new processes and new products in self.A and self.F with regionalized data 
        - Adds substitution to newly created processes
        
        """
        
        # Add columns for new processes
        for p in self.destination.index:
            self.A.loc[:, self.destination.loc[p, 'New process id']]=self.A.loc[:,self.proc_key]
        ## New biofuel combustion process
        self.A.loc[:, self.new_processes.loc['Biofuel combustion', 'New process id']]=self.A.loc[:,self.default_keys.loc['Biofuel combustion', 'Key']]
        ## New sludge incineration process
        self.A.loc[:, self.new_processes.loc['Sludge incineration', 'New process id']]=0*self.A.loc[:, self.default_keys.loc['Biofuel combustion', 'Key']]
        for k in self.sludge_incineration_A.index:
            self.A=self._sp_loc(df=self.A, index = k, columns=self.new_processes.loc['Sludge incineration', 'New process id'], val=self.sludge_incineration_A.loc[k,'value'])
        
        # Add rows for new processes
        self.A_new_columns = self.A.copy(deep = True) # keep a copy of A matrix with new columns
        self.A=np.array(self.A)
        add_row=np.zeros(((self.A.shape[1]-self.A.shape[0]),self.A.shape[1]), dtype=int)
        self.A = np.append(self.A, add_row, axis = 0)
        self.A=pd.DataFrame(data=self.A,index = self.A_new_columns.columns, columns=self.A_new_columns.columns) 
        # Convert self.A to sparse matrix
        self.A = self.A.astype(dtype=pd.SparseDtype(np.float32, 0))
        
        # Remove processes to delete from base process
        self.delete_keys =pd.DataFrame(self.delete.loc[:, self.init_data.loc['Product']])
        self.delete_keys=self.delete_keys.dropna(how='all', axis=0)
        for p in self.destination.index:  
            for d in self.delete_keys[self.init_data.loc['Product']]:
                self.A = self._sp_loc(df=self.A, index = d, columns =self.destination.loc[p, 'New process id'], val = 0)
        
        # Create new processes in self.F
        for p in self.destination.index:
            self.F.loc[:, self.destination.loc[p, 'New process id']]=self.F.loc[:,self.proc_key]*0
        ## Biofuel combustion
        self.F.loc[:, self.new_processes.loc['Biofuel combustion', 'New process id']]=self.F.loc[:,self.default_keys.loc['Biofuel combustion', 'Key']]
        ## Replace fossil emissions by biogenic emissions in biofuel combustion process
        self.F = self._sp_loc(df=self.F, index = self.default_keys.loc['Carbon dioxide, non fossil', 'Key'], columns =self.new_processes.loc['Biofuel combustion', 'New process id'], val = self.F.loc[self.default_keys.loc['Carbon dioxide, fossil', 'Key'],self.new_processes.loc['Biofuel combustion', 'New process id']])
        self.F = self._sp_loc(df=self.F, index = self.default_keys.loc['Carbon monoxide, non fossil', 'Key'], columns =self.new_processes.loc['Biofuel combustion', 'New process id'], val = self.F.loc[self.default_keys.loc['Carbon monoxide, fossil', 'Key'],self.new_processes.loc['Biofuel combustion', 'New process id']])
        self.F = self._sp_loc(df=self.F, index = self.default_keys.loc['Methane, non fossil', 'Key'], columns =self.new_processes.loc['Biofuel combustion', 'New process id'], val = self.F.loc[self.default_keys.loc['Methane, fossil', 'Key'],self.new_processes.loc['Biofuel combustion', 'New process id']])
        self.F = self._sp_loc(df=self.F, index = self.default_keys.loc['Carbon dioxide, fossil', 'Key'], columns =self.new_processes.loc['Biofuel combustion', 'New process id'], val = 0)
        self.F = self._sp_loc(df=self.F, index = self.default_keys.loc['Carbon monoxide, fossil', 'Key'], columns =self.new_processes.loc['Biofuel combustion', 'New process id'], val = 0)
        self.F = self._sp_loc(df=self.F, index = self.default_keys.loc['Methane, fossil', 'Key'], columns =self.new_processes.loc['Biofuel combustion', 'New process id'], val = 0)
        ## Sludge incineration
        self.F.loc[:, self.new_processes.loc['Sludge incineration', 'New process id']] = self.F.loc[:, self.proc_key]*0
        for k in self.sludge_incineration_F.index:
            self.F = self._sp_loc(df=self.F, index = k, columns =self.new_processes.loc['Sludge incineration', 'New process id'], val = -self.sludge_incineration_F.loc[k,'value'])
        
        # Add substitution to new processes and base ecoinvent process in self.A (process are for 1 kg of product in self.A)
        for p in self.destination.index:
            self.A=self._sp_loc(df=self.A, index = self.destination.loc[p, 'Key of substituted product'], columns=self.destination.loc[p, 'New process id'], val=-self.destination.loc[p, 'Substitution ratio'])
        
        self.A=self._sp_loc(df=self.A, index = self.sub_key, columns=self.proc_key, val=-self.init_data.loc['Substitution ratio for base ecoinvent process'])
        
        # Proxy for efficiency
        self.proxy_cluster(data_to_estimate = self.efficiency_data, column_estimate = 'Value', data_tech = self.pib, column_tech = self.year)
        self._change_efficiency()

        # Proxy for electricity consumption
        ## Choice of dataset according to product type
        if self.init_data.loc['Product']=='Newspaper':
            electricity_data = self.news_electricity_consumption
        elif self.init_data.loc['Product']=='Graphic paper':
            electricity_data = self.graphic_electricity_consumption
        else:
            electricity_data = self.occ_electricity_consumption

        self.proxy_cluster(data_to_estimate = electricity_data, column_estimate = 'Value', data_tech = self.pib, column_tech = self.year)
        self._change_mix()

        # Proxy for energy consumption
        ## Choice of dataset according to product type
        if self.init_data.loc['Product']=='Newspaper':
            energy_data = self.news_energy_consumption
        elif self.init_data.loc['Product']=='Graphic paper':
            energy_data = self.graphic_energy_consumption
        else:
            energy_data = self.occ_energy_consumption

        self.proxy_cluster(data_to_estimate = energy_data, column_estimate = 'Value', data_tech = self.pib, column_tech = self.year)
        # Change energy mix and consumption in new processes
        self._change_energ()
        #display(self.A)#6

        # Proxy for water emissions
        ## Choice of dataset according to product type
        if self.init_data.loc['Product']=='Newspaper':
            water_data = self.news_water_emissions
        elif self.init_data.loc['Product']=='Graphic paper':
            water_data = self.graphic_water_emissions
        else:
            water_data = self.occ_water_emissions

        self.water_cluster(water_emissions = water_data, data_tech = self.pib, column_tech = self.year)

        # Proxy for water consumption
        ## Choice of dataset according to product type
        if self.init_data.loc['Product']=='Newspaper':
            water_data2 = self.news_water_consumption
        elif self.init_data.loc['Product']=='Graphic paper':
            water_data2 = self.graphic_water_consumption
        else:
            water_data2 = self.occ_water_consumption

        self.proxy_cluster(data_to_estimate = water_data2, column_estimate = 'Value', data_tech = self.pib, column_tech = self.year)
        self._change_water()
        #display(self.A)#7

        # Proxy with chemicals consumption
        ## Choice of dataset according to product type
        if self.init_data.loc['Product']=='Newspaper':
            chemicals_data = self.news_chemicals_consumption
        elif self.init_data.loc['Product']=='Graphic paper':
            chemicals_data = self.graphic_chemicals_consumption
        else:
            chemicals_data = self.occ_chemicals_consumption
            
        

        self.proxy_cluster(data_to_estimate = chemicals_data, column_estimate = 'Value', data_tech = self.pib, column_tech = self.year)
        self._change_chemicals()
        #display(self.A)#8
        
#___________________________________________________________________________                                   
# PROCESS INSPECTION METHODS                                  
#___________________________________________________________________________  
    def inspect(self, proc_key=None):
        """Inspects the economic flux of a process in A matrix before operation (I-A)
        
        Parameters:
        proc_key: Ecoinvent key-uuid of the process to inspect
        
        Displays:
        self.pro_inspect: DataFrame containing the description of the economic flux of a process.
        
        """
        
        if proc_key is None:
            proc_key = self.proc_key
            
        if proc_key in (self.destination['New process id']).to_list():
            if self.A.shape==self.A_original.shape:
                self._regionalize_A_F()
        
        if proc_key in (self.new_processes['New process id']).to_list():
            if self.A.shape==self.A_original.shape:
                self._regionalize_A_F()
            
        #print(self.pro.loc[proc_key, self.meta])
        a = matrix_view.inspect_col(self.A, proc_key, labels=self.shortlabel, verbose=False, keep_index=False)
        columns = self.meta + ['value']
        self.pro_inspect = pd.DataFrame(a).set_index(0)
        self.pro_inspect.columns = columns  

        display(self.pro_inspect)

#___________________________________________________________________________  
    def _inspect(self, proc_key=None):
        """Inspects the economic flux of a process in A matrix before operation (I-A), but without displaying.
        
        Parameters:
        proc_key: Ecoinvent key-uuid of the process to inspect
        
        * User must run regionalize_A() before using this method with new regionalized processes
        
        """
        
        if proc_key is None:
            proc_key = self.proc_key
            
        if proc_key in (self.destination['New process id']).to_list():
            if self.A.shape==self.A_original.shape:
                self._regionalize_A_F()
        
        if proc_key in (self.new_processes['New process id']).to_list():
            if self.A.shape==self.A_original.shape:
                self._regionalize_A_F()
            
        #print(self.pro.loc[proc_key, self.meta])
        a = matrix_view.inspect_col(self.A, proc_key, labels=self.shortlabel, verbose=False, keep_index=False)
        columns = self.meta + ['value']
        self.pro_inspect = pd.DataFrame(a).set_index(0)
        self.pro_inspect.columns = columns
        
        return(self.pro_inspect)
        
#___________________________________________________________________________
    def inspect_fe(self, proc_key):
        """Inspects the elementary flux of a process in F matrix
        
        Parameters:
        proc_key: Ecoinvent key-uuid of the process to inspect
        
        Displays:
        self.pro_inspect_fe: DataFrame containing the description of the elementary flux of a process.
        
        * User must run regionalize_A() before using this method with new regionalized processes
        
        """
        
        if proc_key is None:
            proc_key = self.proc_key
            
        if proc_key in (self.destination['New process id']).to_list():
            if self.A.shape==self.A_original.shape:
                self._regionalize_A_F()
        
        if proc_key in (self.new_processes['New process id']).to_list():
            if self.A.shape==self.A_original.shape:
                self._regionalize_A_F()
        self.pro_inspect_fe = pd.DataFrame(matrix_view.inspect_col(self.F, proc_key, labels=self.eco['STR'], keep_index=False, verbose=False))
        
        display(self.pro_inspect_fe)
                           
#___________________________________________________________________________                                 
# IMPACT CALCULATION METHODS
#___________________________________________________________________________                                    
    def _define_d_regionalized(self):
        """ Calculates impact results vector (d) for the regionalized scenario (with all exporting destinations provided in the template taken into account) according to the functional unit provided in the template
        
        *User must run regionalize_A before using this method
        
        """
        self.I = sparse.eye(self.A.shape[0])
        # I matrix in sparse format
        self.I = pd.DataFrame.sparse.from_spmatrix(self.I, index=self.A.index, columns=self.A.index) 
        for p in self.destination.index:
            self.destination.loc[p, 'Final demand']= self.init_data.loc['Mass of recycled bale (kg): functional unit']*self.destination.loc[p,'Fraction']/(self.destination.loc[p, 'Total secondary input (kg)']+self.destination.loc[p, 'Total MSW'])  # Unité fonctionnelle basée sur la quantité de ballot avec impuretés entrante
        # Creation of the final demand vector y
        self.y = pd.Series(0, index=self.A.index)
        for p in self.destination.index:
            self.y.loc[self.destination.loc[p,'New process id']] = self.destination.loc[p, 'Final demand']
        # Conversion of y vector in sparse
        self.y = self.y.astype(dtype=pd.SparseDtype(np.float32, 0))
        # Conversion in csc and csr matrix
        self.ImA = csc_matrix((self.I - self.A).sparse.to_coo())
        self.ImA = csr_matrix(self.ImA)
        self.spy = csc_matrix(pd.DataFrame(self.y).sparse.to_coo())
        self.spy = csr_matrix(self.spy)
        # Resolution
        self.CF = self.C_ACV.fillna(0)@self.F.fillna(0)
        self.x = spsolve(self.ImA, self.spy)
        self.x = pd.Series(self.x, index=self.A.index)
        # d_ACV calculation
        self.d = self.CF @ self.x
        
#___________________________________________________________________________        
    def _define_d_1treatment(self, proc_key):
        """ Calculates impact results vector (d) for one wastepaper treatment process (incineration, open dump, landfill, etc.) according to the functional unit provided in the template
        
        Parameters:
        proc_key: Ecoinvent key of the wastepaper treatment process

        * Use function _define_d_1recycling if the wastepaper treatment is a recycling process.
        """
        I = sparse.eye(self.A.shape[0])
        # I matrix in sparse format
        I = pd.DataFrame.sparse.from_spmatrix(I, index=self.A.index, columns=self.A.index)
        #Creation of the final demand vector y
        y = pd.Series(0, index=self.A.index)
        y.loc[proc_key] = (-1)*self.init_data.loc['Mass of recycled bale (kg): functional unit'] # -1 to take into account that ecoinvent gives negative impacts for waste treatments.
        # Conversion of y vector in sparse
        y = y.astype(dtype=pd.SparseDtype(np.float32, 0))
        # Conversion in csc and csr matrix
        ImA = csc_matrix((I - self.A).sparse.to_coo())
        ImA = csr_matrix(ImA)
        spy = csc_matrix(pd.DataFrame(y).sparse.to_coo())
        spy = csr_matrix(spy)
        # Resolution
        CF = self.C_ACV.fillna(0)@ self.F.fillna(0)
        x = spsolve(ImA, spy)
        x = pd.Series(x, index=self.A.index)
        # d calculation
        self.d1t = CF @ x
        
#___________________________________________________________________________        
    def _define_d_1recycling(self, proc_key=None):
        """ Calculates impact results vector (d) for the base ecoinvent recycling process or a new regionalized recycling process (for one country) according to the functional unit provided in the template
        
        Parameters:
        proc_key: Ecoinvent key of the wastepaper recycling process
        
        * Includes transport and substitution specified in the template for regionalized processes
        * Includes substitution for base ecoinvent process

        """
        if proc_key is None:
            proc_key = self.proc_key
            
        self._inspect(proc_key)
        self.mae = self.pro_inspect.loc[self.rawmat_key, 'value']
        
        I = sparse.eye(self.A.shape[0])
        # I matrix in sparse format
        I = pd.DataFrame.sparse.from_spmatrix(I, index=self.A.index, columns=self.A.index)
        #Creation of the final demand vector y
        y = pd.Series(0, index=self.A.index)
        y.loc[proc_key] = (1/self.mae)*self.init_data.loc['Mass of recycled bale (kg): functional unit'] 
        # Conversion of y vector in sparse
        y = y.astype(dtype=pd.SparseDtype(np.float32, 0))
        # Conversion in csc and csr matrix
        ImA = csc_matrix((I - self.A).sparse.to_coo())
        ImA = csr_matrix(ImA)
        spy = csc_matrix(pd.DataFrame(y).sparse.to_coo())
        spy = csr_matrix(spy)
        # Resolution
        CF = self.C_ACV.fillna(0)@self.F.fillna(0)
        x = spsolve(ImA, spy)
        x = pd.Series(x, index=self.A.index)
        # d calculation
        self.d1r = CF @ x
        
#___________________________________________________________________________       
    def define_contribution(self, proc_key):
        """Creates a matrix containing all the economic activities contribution to the impacts of one regionalized recycling process

        Parameters:
        pro_inspect: the resulting DataFrame of the inspect() method
        proc_key: the uuid of the regionalized process to analyze

        """
        pro_inspect = self._inspect(proc_key)
        mae=1
        A_ACV=self.A
        C_ACV = self.C_ACV
        F_ACV = self.F
        I = sparse.eye(A_ACV.shape[0])
        I = pd.DataFrame.sparse.from_spmatrix(I, index=A_ACV.index, columns=A_ACV.index)
        Y = pd.DataFrame(index=A_ACV.index)
        for p in pro_inspect.index:
            Y.loc[p,p]=(A_ACV.loc[p, proc_key])/mae
        Y= Y.fillna(0.0)
        spY = csr_matrix(Y)
        ImA = csc_matrix((I - A_ACV).sparse.to_coo())
        ImA = csr_matrix(ImA)
        X = pd.DataFrame(index=A_ACV.index, columns=Y.columns)
        X=spsolve(ImA, spY)
        X_df = pd.DataFrame.sparse.from_spmatrix(X, index = A_ACV.index, columns = Y.columns)
        CF = C_ACV.fillna(0)@F_ACV.fillna(0)
        D = CF @ X_df
        y = pd.Series(0, index=A_ACV.index)
        y.loc[proc_key] = 1/mae
        D.loc[:, proc_key]=CF@y

        D = D.drop(proc_key, axis=1)

        for i in D.columns:
            if i in pro_inspect.index:
                D.loc['Activity name', i]=pro_inspect.loc[i, 'activityName']+' ;'+pro_inspect.loc[i, 'productName']+' ;'+pro_inspect.loc[i, 'geography']

        # Graphic production
        D_name = D.T.set_index('Activity name').T
        ## Simplification
        D_simpl = pd.DataFrame(index=D_name.index)
        for imp in D_name.index:
            D_larg=abs(D_name).T.sort_values(imp, ascending=False).head(4)
            for i in D_larg.index:
                D_simpl.loc[imp,i]=D_name.loc[imp,i]
        D_simpl.loc[:,'Others']=D_name.T.sum()-D_simpl.T.sum()
        D_simpl = D_simpl.fillna(0)
        ## Normalisation
        D_norm = pd. DataFrame(index=D_simpl.index, columns = D_simpl.columns)
        for imp in D_simpl.index:
            for p in D_simpl.columns:
                D_norm.loc[imp, p]=D_simpl.loc[imp, p]/abs(D_simpl).T.sum().loc[imp]
        ##Graph
        plt.rcParams['font.size'] = '25'
        graph_cont = D_norm.plot(kind='barh',figsize=(40,25), width=0.5, stacked=True, color = ['turquoise', 'orange','green', 'purple','yellow','lightpink','deeppink','yellowgreen','indigo','red', 'gainsboro', 'darkblue','gold','black'])
        plt.xlim(-1,1)
        graph_cont.legend(loc = 'best', bbox_to_anchor = (1.0,0.5), prop={'size': 20})
        self.graph_cont = graph_cont
        self.D_contribution = D_name
        
#___________________________________________________________________________                                 
# SCENARIO: NO CHANGE TO BASE PROCESS
#___________________________________________________________________________
    def scenario_ecoinvent(self):
        """Creates d impact vector for the ecoinvent base case scenario, according to the functional unit specified in the template
        
        Returns:
        d_scenario_ecoinvent: d vector for the ecoinvent base case scenario
        
        """
        if self.A.shape==self.A_original.shape:
            self._regionalize_A_F()
        self._define_d_1recycling()
        self.d_scenario_ecoinvent = self.d1r
        
        return self.d_scenario_ecoinvent
#___________________________________________________________________________                                 
# SCENARIO: REGIONALIZED CASE WITH ALL DESTINATIONS SPECIFIED IN THE TEMPLATE
#___________________________________________________________________________
    def scenario_regionalized(self):
        """Creates d vector for the regionalized scenario comprising all destinations, according to the functional unit specified in the template
        
        Returns:
        d_scenario_regionalized: d vector for the regionalized scenario comprising all destinations.
        
        """
        if self.A.shape==self.A_original.shape:
            self._regionalize_A_F()
            
        self._define_d_regionalized()
        self.d_scenario_regionalized = self.d
        
        return self.d_scenario_regionalized
#___________________________________________________________________________                                 
# SCENARIO: ONE CHOSEN WASTE TREATMENT WITHOUT SUBSTITUTION
#___________________________________________________________________________
    def scenario_other_treatment(self, treatment_key=None):
        """Creates d vector for the other waste treatment (incineration, landfill, etc.) without substitution, according to the functional unit specified in the template
        
        Parameters: 
        treatment_key: Ecoinvent key of the waste treatment process 
        
        Returns:
        d_scenario_other_treatment: d vector for the other waste treatment without substitution
        
        * Default treatment key is the one provided in the "Initial data" label of the template
        
        """
        if treatment_key is None:
            treatment_key = self.init_data.loc['Waste treatment process key']
        
        if self.A.shape==self.A_original.shape:
            self._regionalize_A_F()
            
        self._define_d_1treatment(proc_key = treatment_key)
        self.d_scenario_other_treatment = self.d1t
        
        return self.d_scenario_other_treatment

#___________________________________________________________________________                                 
# SCENARIO: ONE CHOSEN REGIONALIZED RECYCLING PROCESS
#___________________________________________________________________________
    def scenario_recycling(self, recycling_key):
        """Creates d vector for one chosen regionalized recycling process, according to the functional unit specified in the template
        
        Parameters: 
        recycling_key: Ecoinvent key of the new recycling process (can be newly created recycling processes, but will include transport and substitution as specified in the "Destination" label of the template) 
        
        Returns:
        d_scenario_recycling: d vector for one chosen regionalized recycling process
        
        """
            
        if self.A.shape==self.A_original.shape:
            self._regionalize_A_F()
            
        self._define_d_1recycling(proc_key = recycling_key)
        self.d_scenario_recycling = self.d1r
        
        return self.d_scenario_recycling

#___________________________________________________________________________                                 
# SCENARIO: ALL CHOSEN REGIONALIZED RECYCLING PROCESS
#___________________________________________________________________________
    def scenario_recycling_all(self):
        """Creates D matrix containing all chosen regionalized recycling processes in the "Destination" label of the template, according to the functional unit specified in the template
        
        Returns:
        D_matrix: D matrix for all chosen regionalized recycling process
        
        """
        self.D_matrix = pd.DataFrame(index = self.C_ACV.index)
        
        if self.A.shape==self.A_original.shape:
            self._regionalize_A_F()
            
        for p in self.destination.index: 
            self._define_d_1recycling(proc_key = self.destination.loc[p,'New process id'] )
            self.D_matrix.loc[:,self.destination.loc[p,'Country code']] = self.d1r
            
        self.D_matrix = self.D_matrix.T
        
        self.D_matrix['Country code']=self.D_matrix.index
        
        return self.D_matrix
    
#___________________________________________________________________________                                 
# COMPARATIVE GRAPH GENERATION
#___________________________________________________________________________
    def scenarios_excel_graph(self, list_d, list_names):
        """Creates an excel file named Results containing d vectors for all scenarios and generates a comparative normalized graph to show the impacts of the scenarios
        
        Parameters:
        list_d: List of d vectors to show in the graph (a list in inside brackets [], with commas to separate the vectors)
        list_names: List of the name given to every scenario represented
        
        *User must have filled the "Short impacts" label in the template with the chosen impact categories for the graph to be generated.
        
        """
        
        # Create d_total
        self.d_total = pd.concat(list_d, axis=1)
        self.d_total.columns = list_names
        
        # Save LCA results for every scenario in Excel file
        excel = pd.ExcelWriter('Results.xlsx', engine='xlsxwriter')
        self.d_total.to_excel(excel, sheet_name = 'results')
        excel.save()

        self.d_total=self.d_total.sort_index(ascending=False)

        self._trace_graph_comp(self.d_total)
            


# In[ ]:




