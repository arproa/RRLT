import re
import warnings
import numpy as np
import pandas as pd
from pandas.testing import assert_frame_equal
import pdb

def abbreviate(a):
    """ Removes null rows and columns"""

    # Remove null rows first
    a = a.loc[a.sum(1) != 0.0, :]

    # then remove null columns
    a = a.loc[:, a.sum(0) != 0.0]

    return a

def insert_values_except_nans(df, dfins):

    msg = ''

    # Create a dataframe identical to dfins except with its NaNs replace by entries from df
    ins = df.reindex_like(dfins).where(dfins.isna(), other=dfins)

    # Insert this dataframe
    df = df.copy()
    try:
        df.loc[ins.index, ins.columns] = ins
    except KeyError as err:
        msg = str(err)

    if msg:
        raise KeyError("Some dimensions in inserted property layer not found in original layer. Not allowed with 'update_*' option." + msg)

    return df


def eye_like(df):
    """ Generate an identity matrix similar to a dataframe"""
    I = np.eye(df.shape[0], df.shape[1])
    I = pd.DataFrame(I, index=df.index, columns=df.index)
    return I

def df_inv(df):
    """ Perform inverse, and restore DataFrame indexes once done"""
    return pd.DataFrame(np.linalg.inv(df), index=df.index, columns=df.index)


def pd_allclose(a, b):
    """ Test if two Pandas DataFrames or Series are all close

    This function does **not** check the order of the indexes and columns, nor
    does it test for exact equality (rounding errors)

    Parameters
    ----------
    a, b: DataFrames or Series
       Variables to be compared

    Returns
    -------
        True if all coefficients close. False if Dataframes differ, or if
        variables being compared cannot even be converted to a dataframe

    """
    try:
        pd.testing.assert_frame_equal(pd.DataFrame(a), pd.DataFrame(b), check_like=True, check_exact=False)
        return True
    except AssertionError:
        return False
    except ValueError:
        # Not even dataframeable
        return False

def series_allclose(a, b, allow_empty=True, nanisnull=False):

    # If empty series is not allowed, then the prensence of an empty series returns a fals comparison
    if not allow_empty:
        if not len(a) or not len(b):
            return False

    # Preprocess if NaN can equate 0. Reindex all and replace by zero
    if nanisnull:
        ix = a.index.union(b.index)
        a = a.reindex(ix).fillna(0.)
        b = b.reindex(ix).fillna(0.)

    # In any other case, comparison proceeds
    try:
        pd.testing.assert_series_equal(a, b, check_names=False)
        return True
    except AssertionError:
        return False


def one_over(r):
    """ return 1/r except r=0, then return 0 """
    with np.errstate(divide='ignore'):
        one_over_r = 1.0 / r
    one_over_r[r == 0.0] = 0.0
    return one_over_r


def gt_significantly(a, b, rtol=1e-05, atol=1e-08):
    """ Are all entries of A significantly greater than those of B? """
    out = False

    # if without any close
    if ~ np.any(np.isclose(a, b, rtol=rtol, atol=atol)):
        # and all those significant differences are positive
        if np.all(a > b):
            out = True
    return out


def lt_significantly(a, b, rtol=1e-05, atol=1e-08):
    """ Are all entries of A significantly smaller than those of B? """
    out = False

    # if without any close
    if ~ np.any(np.isclose(a, b, rtol=rtol, atol=atol)):
        # and all those significant differences are positive
        if np.all(a < b):
            out = True
    return out

def pd_isnull(x):

    if x is None:
        out = True
    elif len(x) == 0:
        out = True
    else:
        out = False

    return out

def dropnull(x, cols=True, rows=True):
    """ Drop all NaN/0 entries in dataframe or series. In series, remove also strings """

    if x.ndim == 1:
        y = pd.to_numeric(x, errors='coerce').dropna()
        out = y[y != 0]

    else:  # Applies to a dataframe
        test = x.fillna(0) != 0
        if cols:
            keep_cols = (test).any(axis=0)
        else:
            keep_cols = x.columns

        if rows:
            keep_rows = (test).any(axis=1)
        else:
            keep_rows = x.index

        out = x.loc[keep_rows, keep_cols]

    return out


def notnull(x):

    if isnan(x) or x == 0:
        out = False
    else:
        out = True

    return out

def isnan(x):
    """ Robust isnan: if not coercible in isnan, then is not NaN"""

    # Default False
    verdict = False

    if x is None:
        verdict = True
    else:
        try :
            verdict = np.isnan(x)

        except TypeError:  # Probably a string
            try:
                verdict = x.casefold() == 'nan'

            except AttributeError: # Maybe not a string afterall, but not nan either
                pass

    return verdict

def flexdot(a, b):


    # Initialize default (empty) output
    if a.ndim == 1 or b.ndim ==1:
        out = pd.Series()
    else:
        out = pd.DataFrame()


    # Get relevant indexes for an inner product in a and b
    if a.ndim == 2:
        ix_a = set(a.columns)
    elif a.ndim == 1:
        ix_a = set(a.index)
    ix_b = set(b.index)

    # Inner product only affected by common indexes, drop the rest
    if ix_a != ix_b:
        ix = list(ix_a & ix_b)
        if ix:
            out = a[ix].fillna(0.) @ b.loc[ix].fillna(0.)
    else:
        out = a.fillna(0.) @ b.fillna(0.)

    return out

def harmonize(table_list, fill_value=None):

    # If not all tables have the same shape, do something
    if len(set(t.shape for t in table_list)) != 1:

        # Fill_value
        if fill_value is None:
            fill_value = np.nan

        # Find union of index
        ix = table_list[0].index
        for t in table_list[1:]:
            ix = ix.union(t.index)

        # If all items have columns, get union, and then reindex accordingly
        if set([t.ndim for t in table_list]) == set([2]):
            cols = table_list[0].columns
            for t in table_list[1:]:
                cols = cols.union(t.columns)

            table_list = [t.reindex(index=ix, columns=cols, fill_value=fill_value) for t in table_list]

        # If all items are Series, reindex only index
        elif set([t.ndim for t in table_list]) == set([1]):
            table_list = [t.reindex(index=ix, fill_value=fill_value) for t in table_list]

        else:
            raise ValueError("Inconsistent dimensions. Cannot hamornize")

    return table_list

def ddiag(a, nozero=False):
    """ Robust diagonalization : always put selected diagonal on a diagonal!

    This small function aims at getting a behaviour closer to the
    mathematical "hat", compared to what np.diag() can delivers.

    If applied to a vector or a 2d-matrix with one dimension of size 1, put
    the coefficients on the diagonal of a matrix with off-diagonal elements
    equal to zero.

    If applied to a 2d-matrix (with all dimensions of size > 1), replace
    all off-diagonal elements by zeros.

    Parameters
    ----------
    a : numpy matrix or vector to be diagonalized

    Returns
    --------
    b : Diagonalized vector

    Raises:
       ValueError if a is more than 2dimensional

    See Also
    --------
        diag
    """

    # If numpy vector
    if a.ndim == 1:
        b = np.diag(a)

    # If numpy 2d-array
    elif a.ndim == 2:

        #...but with dimension of magnitude 1
        if min(a.shape) == 1:
            a = np.squeeze(a)
            b = np.diag(a)

        # ... or a "true" 2-d matrix
        else:
            b = np.diag(np.diag(a))

    else:
        raise ValueError("Input must be 1- or 2-d")

    # Extreme case: a 1 element matrix/vector
    if b.ndim == 1 & b.size == 1:
        b = b.reshape((1, 1))



    if nozero:
        # Replace offdiagonal zeros by nan if desired
        c = np.empty_like(b) *  np.nan
        di = np.diag_indices_from(c)
        c[di] = b.diagonal()
    else:
        c = b

    # If pandas dataframe, preserve labels
    try:
        c = pd.DataFrame(c, a.index.values, a.columns.values)
    except AttributeError:
        try:
            c = pd.DataFrame(c, a.index.values, a.index.values)
        except AttributeError:
            pass



    # A certainly diagonal vector is returned
    return c

# ----------------------
# aliases
dot = flexdot
