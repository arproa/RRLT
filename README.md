# Ph.D. Arianne

## Objective 1: Parametrized regionalization of paper recycling life-cycle assessment
Folders:
- Templates: Contains the base template (Base_template.xlsx) to fill for custom LCA computation (already filled with examples to show how to use it; use it after importing the module recycling_scenarios3.py), and the templates used to generate all results.

Files:
- Data IEA.ipynb: Aggregation of IEA energy data from pulp and paper sector in different energy types categories
- Instructions.docx: Instructions to fill the Base_template.xlsx template.
- Test_module_import.ipynb: an example of how to use the module recycling_scenario3.py
- recycling_scenario3.ipynb: Code to perform different LCAs based on the template. recycling_scenario3.py module also available.
- energy-consumption-by-source-and-region.csv: Energy consumption by source and region from U.S. Energy Information Administration Database
- iea_results.xlsx: IEA energy consumption for pulp and paper sector aggregated in energy source categories for countries available in IEA database.

## Objective 2

## Objective 3
